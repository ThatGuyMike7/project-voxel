TODO

- Check for neighbouring Chunks in chunk generation
- Implement custom MSAA/FXAA solution

- Finish SlotMap template
(- Auto-resize internal vector when the end is reached or if the amount of data is much smaller than the vector size)

- Split World Generation into own classes and modules

- Dynamically change WorldGenerator's `heightPassY` based on some seed to allow even more combinations and constellations of the heightmap

- Generate Cell (16x256x16) and save the data to file. When a Chunk (16x16x16) needs access to the world generation data, it must read from the file.


DEBUG

- Frustum Culling: when close to the ground, you can still see chunks disappearing -> reduce nearPlane?
