cd build
cmake -G "Visual Studio 15" -D CMAKE_GENERATOR_PLATFORM=x64 -D CMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release

:: /w is for waiting until the process closes, /b is for running in the same console window
;; /d sets the working directory

cd ..
start /d "." /w /b bin/x64/Release/ProjectVoxel.exe