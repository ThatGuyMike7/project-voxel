#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "Shader.h"
#include "Transform.h"

// Vertex representation with position, color and UV
struct Vertex
{
	Vertex() {}
	Vertex(const glm::vec3& pos)
	{
		this->pos = pos;
	}
	Vertex(const glm::vec3& pos, const glm::vec2& uv)
	{
		this->pos = pos;
		this->uv = uv;
	}
	Vertex(const glm::vec3& pos, const glm::vec2& uv, const glm::vec3& normal)
	{
		this->pos = pos;
		this->uv = uv;
		this->normal = normal;
	}

	glm::vec3 pos;
	glm::vec2 uv;
	glm::vec3 normal;
};

// Buffer data to be sent to OpenGL
struct BufferData
{
	BufferData() {}

	BufferData(const std::vector<Vertex>& vertices)
	{
		this->vertices = vertices;
	}

	inline virtual void clear() { vertices.clear(); }

	std::vector<Vertex> vertices;
};

// Builds a drawable mesh with an assignable shader and destroys the mesh in the destructor
class Mesh
{
public:

	static int DRAW_CALLS;

	// The mesh's shader is not assigned. Assign it before rendering or it could lead to undefined behaviour.
	Mesh();
	// Set the mesh's shader. It must never be nullptr if you attempt to draw the mesh.
	Mesh(Shader* shader);
	virtual ~Mesh();

	Mesh(const Mesh& mesh) = delete;
	Mesh& operator=(const Mesh& mesh) = delete;

	// Move constructor
	Mesh(Mesh&& other) : shader(std::move(other.shader)), VAO(std::move(other.VAO)), VBO(std::move(other.VBO)), vertexCount(std::move(other.vertexCount)), transform(std::move(other.transform))
	{
		// Invalidate old mesh
		other.shader = nullptr;
		other.VAO = 0;
		other.VBO = 0;
		other.vertexCount = 0;
		other.transform = Transform{};
	}
	//Mesh(Mesh&& other) = default;

	// Move assignment operator
	Mesh& operator=(Mesh&& other)
	{
		shader = std::move(other.shader);
		VAO = std::move(other.VAO);
		VBO = std::move(other.VBO);
		vertexCount = std::move(other.vertexCount);
		transform = std::move(other.transform);

		// Invalidate old mesh
		other.shader = nullptr;
		other.VAO = 0;
		other.VBO = 0;
		other.vertexCount = 0;
		other.transform = Transform{};

		return *this;
	}
	//Mesh& operator=(Mesh&& other) = default;

	// Build the mesh by sending buffer data to the GPU
	void build(const BufferData& data);

	// Draw the mesh's VAO. Does not include using the shader, so do that first.
	virtual void draw();

	Shader* getShader() const;

	// Change the shader used for rendering this mesh. Returns false if the passed shader is nullptr.
	bool setShader(Shader* shader);

	Transform transform;

protected:

	// Create all the necessary buffer objects and upload them to OpenGL.
	// The VAO is bound before this function is called, in the 'build' method.
	virtual void createBuffers(const BufferData& data);

	// The shader that should be used to render this mesh
	Shader* shader;

	// Vertex Array Object which is indirectly bound to VBO and EBO
	unsigned int VAO;

	// Vertex Buffer Object
	unsigned int VBO;

	// How many vertices this mesh has
	unsigned int vertexCount;
};
