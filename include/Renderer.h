#pragma once

#include <vector>

#include "Camera.h"
#include "Mesh.h"

// Renderer for drawing chunks and other entities
class Renderer
{
public:
	Renderer(Camera* camera);
	~Renderer();

	Renderer(const Renderer& rend) = delete;
	Renderer& operator=(const Renderer& rend) = delete;

	Mesh& moveMesh(Mesh&& mesh);

	// Draw all meshes by iterating over every single one and making a draw call to OpenGL.
	// They must all use the same shader!
	void bruteDraw(Shader* shader);

private:
	std::vector<Mesh> meshList;

	Camera* camera;
};

