#pragma once

#include <noise/noise.h>
#include <memory>
#include <vector>
#include <utility> // for std::pair

#include "Seed.h"
#include "Biome.h"

namespace gen
{
	// Generates biomes and contains utility functions for generating chunks
	class WorldGenerator
	{
		friend class World;

	public:
		WorldGenerator(Seed seed = 51525, double heightPassY = 0.0, double heightScale = 128.0);
		virtual ~WorldGenerator();

		// Determine the height of a block depending on its 2D position in the biome map (height pass).
		// Returns the height in blocks and the index of the biome.
		std::pair<int, int> getBlockHeight(int blockX, int blockZ);

		// Determine the height of a block depending on its 2D position in the biome map (height pass).
		// Returns the noise value [-1.0; 1.0] of the heightmap and the index of the biome.
		std::pair<double, int> getNoiseHeight(int blockX, int blockZ);

		// Returns the temperature, humidity and elevation at a given block position
		Ecocline getEcoAtBlock(int blockX, int blockZ) const;

		// Returns the temperature, humidity and elevation at a given noise sample
		Ecocline getEcoAtNoise(double noiseX, double noiseZ) const;

		// Returns the normalized weights of all the biomes at a block and the index of the biome this block is a part of.
		// The weights of all biomes thus add up to 1, and the higher it is, the more the block resembles a part of that biome.
		std::pair<std::vector<double>, int> getWeights(int blockX, int blockZ);

		// Returns all biome types
		inline const std::vector<std::unique_ptr<biome::Biome>>& getBiomes() const { return biomes; }

		// (3) Create .png file of a part of the biome map with the heights determining the pixel color
		bool writeBiomeMapHeights(int originX, int originZ, int width, int depth, const char* filename);
		bool writeBiomeMapHeightsAndSectors(int originX, int originZ, int width, int depth, const char* filename);

		// (2) Create .png file of a part of the biome map with distinguishable colors for each biome
		bool writeBiomeMapSectors(int originX, int originZ, int width, int depth, const char* filename);

		// (1) Create .png file of a part of the biome map in RGB. Returns false if file failed to save.
		bool writeBiomeMapRGB(int originX, int originZ, int width, int depth, const char* filename);
		
	private:

		// All biomes that the world generator uses
		std::vector<std::unique_ptr<biome::Biome>> biomes;

		// Biome map generator modules
		noise::module::Perlin temperatureMapPerlin;
		noise::module::Perlin humidityMapPerlin;
		noise::module::Perlin elevationMapPerlin;

		noise::module::ScalePoint temperatureMap, humidityMap, elevationMap;
		
		// Heightmap value [-1.0; 1.0] multipled by `heightScale` returns the height in blocks
		double heightScale;

		// Which Y layer to pick for the 2D heightmap (in noise coords!)
		double heightPassY;

		Seed seed;

		template<typename T>
		constexpr inline T square(T x)
		{
			return x * x;
		}
	};
}
