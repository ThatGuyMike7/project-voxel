#pragma once

#include "Chunk.h"

// How many chunks a cell represents on Y
const unsigned int CELL_ONE = 16u;

namespace gen
{
	// Generates and holds the data of `CELL_ONE` chunks along the Y axis for world generation purposes
	class Cell
	{
	public:
		Cell();
		~Cell();
	};
}