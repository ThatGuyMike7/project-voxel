#pragma once

#include "DeltaTime.h"
#include "SlotMap.h"
#include <vector>
#include <map>
#include <iostream>

void slotmap_benchmark()
{
	const int NUM = 50000;

	SlotMap<short, std::string> slotmap{ NUM + 100 };
	std::vector<std::string> vec;
	std::map<int, std::string> mymap;

	DeltaTime time;

	/// PUSH TIME
	std::cout << "Testing Slotmap Push DeltaTime: ";
	time.reset();
	for (int i = 0; i < NUM; i++)
	{
		slotmap.add(i, "slotmapitem_" + std::to_string(rand()));
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	std::cout << "Testing Vector Push DeltaTime: ";
	time.reset();
	for (int i = 0; i < NUM; i++)
	{
		vec.push_back("vectoritem_" + std::to_string(rand()));
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	std::cout << "Testing Map Push DeltaTime: ";
	time.reset();
	for (int i = 0; i < NUM; i++)
	{
		mymap[i] = "mapitem_" + std::to_string(rand());
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	/// ACCESS TIME
	std::cout << "Testing Slotmap Iteration DeltaTime: ";
	time.reset();
	/*for (int i = 0; i < NUM; i++)
	{
		auto obj = slotmap.get(i);
		if (*obj)
		{
			obj->value();
		}
	}*/
	for (auto it = slotmap.begin(); it != slotmap.end(); it++)
	{
		if (*it)
		{
			it->value();
		}
	}
	/*for (auto& it : slotmap) // This is the fastest iteration method
	{
		if (it)
		{
			it.value();
		}
	}*/
	time.calc();
	std::cout << time.getDelta() << std::endl;

	std::cout << "Testing Vector Iteration DeltaTime: ";
	time.reset();
	for (auto it = vec.begin(); it != vec.end(); it++)
	{
		*it;
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	std::cout << "Testing Map Iteration DeltaTime: ";
	time.reset();
	for (int i = 0; i < mymap.size(); i++)
	{
		mymap.at(i);
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	/// REMOVE TIME
	std::cout << "Testing Slotmap Remove DeltaTime: ";
	time.reset();
	for (int i = 0; i < NUM; i++)
	{
		slotmap.remove(i);
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	std::cout << "Testing Vector Remove DeltaTime: ";
	time.reset();
	for (int i = 0; i < NUM; i++)
	{
		//vec.pop_back();
		vec.erase(vec.end() - 1);
		//vec.erase(vec.begin());  // Worst Case
	}
	time.calc();
	std::cout << time.getDelta() << std::endl;

	std::cout << "Testing Map Remove DeltaTime not possible" << std::endl;
	//time.reset();
	//for (int i = 0; i < 50000; i++)
	//{
	//	
	//}
	//time.calc();
	//std::cout << time.getDelta() << std::endl;
}