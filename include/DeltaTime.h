#pragma once

// Deltatime calculation and time scaling functionality
class DeltaTime
{
public:
	DeltaTime(float timeScale = 1.0f);
	~DeltaTime();

	DeltaTime(const DeltaTime& time) = delete;
	DeltaTime& operator=(const DeltaTime& time) = delete;

	float timeScale;

	// Internally sets 'lastTime' to glfw time and delta time to 0. Call this before the game loop to prevent a big delta time
	// that determines how long the game has been loading as the very first value.
	void reset();

	// Calculate a new delta time
	void calc();

	// Returns the delta time of the last call to 'calc()' multipled by 'timeScale'
	inline float getDelta()
	{
		return deltaTime;
	}

private:

	// DeltaTime elapsed since start of program until the last frame
	float lastTime;

	// Delta time of the last and current frame multipled by 'timeScale'
	float deltaTime;
};

