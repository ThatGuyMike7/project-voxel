#pragma once

#include <string>
#include <glm/vec2.hpp>
#include <unordered_map>

// Size of a block in all 3 dimensions
const float BLOCK_ONE = 1.0f;
// Size of a block in all 3 dimensions divided by two
const float BLOCK_HALF = BLOCK_ONE / 2.0f;

// Block holding data like texture coords, resistance etc.
struct Block
{
public:
	// All blocks in the game. 
	// Refer to a block by passing the block name as the key. 
	// Blocks can be added easily with this unordered map, and access speed is faster than
	// from a regular map container.
	static std::unordered_map<std::string, Block> BLOCKS;

	Block();
	Block(const std::string& name, const glm::vec2& uv);
	~Block();

	std::string name;
	glm::vec2 uv;
};