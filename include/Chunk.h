#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include <noise/noise.h>

#include "Block.h"
#include "Mesh.h"
#include "Tileset.h"

// How many blocks there are in a chunk on each axis
const unsigned int CHUNK_ONE = 16;
//const float CHUNK_ONEF = static_cast<float>(CHUNK_ONE);

// Size of a chunk in one axis in world coordinates
const float CHUNK_SIZE = BLOCK_ONE * static_cast<float>(CHUNK_ONE);

// Forward-declare world, include is in the .cpp
class World;

struct ChunkKey
{
	ChunkKey()
		: x(0), y(0), z(0)
	{ }

	ChunkKey(int x, int y, int z) 
		: x(x), y(y), z(z)
	{ }

	// `std::map` expects `operator<` to be implemented to determine where to insert elements
	bool operator<(const ChunkKey& right) const
	{
		return std::tuple(x, y, z) < std::tuple(right.x, right.y, right.z);
		//return std::tie(x, y, z) < std::tie(right.x, right.y, right.z);
		//return x < right.x && y < right.y && z < right.z;
		//return true;
		//return x + y * z < right.x + right.y * right.z;
		//return (x + y + z) < (right.x + right.y + right.z);
	}

	ChunkKey& operator+=(const ChunkKey& right)
	{
		x += right.x;
		y += right.y;
		z += right.z;
		return *this;
	}

	friend ChunkKey operator+(ChunkKey left, const ChunkKey& right)
	{
		left += right;
		return left;
	}

	int x;
	int y;
	int z;
};

// Representation of CHUNK_ONE^3 blocks
class Chunk
{
public:
	// Empty chunk without world and shader selected, these have to be set manually or undefined behaviour can occur!
	Chunk();

	Chunk(const ChunkKey& key, World* world);
	~Chunk();

	Chunk(const Chunk& chunk) = delete;
	Chunk& operator=(const Chunk& chunk) = delete;

	// Move constructor
	Chunk(Chunk&& other) : blocks(std::move(other.blocks)), world(std::move(other.world)), mesh(std::move(other.mesh)),
		frontChunk(std::move(other.frontChunk)), backChunk(std::move(other.backChunk)), leftChunk(std::move(other.leftChunk)),
		rightChunk(std::move(other.rightChunk)), upChunk(std::move(other.upChunk)), downChunk(std::move(other.downChunk)),
		loaded(std::move(other.loaded))
	{
		// Invalidate old chunk
		other.blocks = nullptr;
		other.world = nullptr;
		other.loaded = false;

		other.frontChunk = ChunkKey{ 0, 0, 0 };
		other.backChunk = ChunkKey{ 0, 0, 0 };
		other.leftChunk = ChunkKey{ 0, 0, 0 };
		other.rightChunk = ChunkKey{ 0, 0, 0 };
		other.upChunk = ChunkKey{ 0, 0, 0 };
		other.downChunk = ChunkKey{ 0, 0, 0 };
	}
	//Chunk(Chunk&& other) = delete;

	// Move assignment operator
	Chunk& operator=(Chunk&& other)
	{
		// Check for self-assignment
		if (this == &other)
			return *this;

		blocks = std::move(other.blocks); 
		world = std::move(other.world);
		mesh = std::move(other.mesh);
		loaded = std::move(other.loaded);

		frontChunk = std::move(other.frontChunk); 
		backChunk = std::move(other.backChunk); 
		leftChunk = std::move(other.leftChunk);
		rightChunk = std::move(other.rightChunk); 
		upChunk = std::move(other.upChunk); 
		downChunk = std::move(other.downChunk);


		// Invalidate old chunk
		other.blocks = nullptr;
		other.world = nullptr;
		other.loaded = false;

		other.frontChunk = ChunkKey{ 0, 0, 0 };
		other.backChunk = ChunkKey{ 0, 0, 0 };
		other.leftChunk = ChunkKey{ 0, 0, 0 };
		other.rightChunk = ChunkKey{ 0, 0, 0 };
		other.upChunk = ChunkKey{ 0, 0, 0 };
		other.downChunk = ChunkKey{ 0, 0, 0 };

		return *this;
	}
	//Chunk& operator=(Chunk&& other) = delete;

	// Returns the eight corner points of the chunk in world coordinates
	std::vector<glm::vec3> getCorners() const;

	// Create 3D blocks array and fill it with data provided by the WorldGenerator member of World
	void load();

	// Build the chunk's mesh (must call load() first)
	void build();

	// Rebuilds the neighbouring chunk meshes
	void rebuildNeighbours();

	void print();

	// Returns a block in the chunk with the given position
	inline Block& getBlock(int x, int y, int z) const { return blocks[index(x, y, z)]; }

	// The chunk's key in the `chunks` slotmap. It is also the position of the chunk in the world (measured in chunks).
	inline ChunkKey getKey() const { return key; }
	
	// Whether or not block data has been generated for this chunk and is available
	inline bool isLoaded() { return loaded; }

	/// Get neighbour chunks
	inline ChunkKey getFrontChunk() const { return frontChunk; }
	inline ChunkKey getBackChunk() const { return backChunk; }
	inline ChunkKey getLeftChunk() const { return leftChunk; }
	inline ChunkKey getRightChunk() const { return rightChunk; }
	inline ChunkKey getUpChunk() const { return upChunk; }
	inline ChunkKey getDownChunk() const { return downChunk; }
	///

	static unsigned int index(int x, int y, int z) { return x + CHUNK_ONE * (y + CHUNK_ONE * z); }

	Mesh mesh;

private:

	// Whether or not block data has been generated for this chunk and is available
	bool loaded;

	// World that contains this chunk and stores the chunk shader and tileset.
	// This allows access to all other currently loaded chunks (for checking neighbours).
	World* world;

	// The chunk's key in the `chunks` slotmap. It is also the position of the chunk in the world (measured in chunks).
	ChunkKey key;

	/// Neighbour chunks
	ChunkKey frontChunk;
	ChunkKey backChunk;
	ChunkKey leftChunk;
	ChunkKey rightChunk;
	ChunkKey upChunk;
	ChunkKey downChunk;
	///

	// 3D Array of blocks, implemented in a single dynamic array for contiguous memory
	Block* blocks;

	// Filled with vertices when the chunk's mesh is being built
	BufferData bufferData;

	// Face functions take the block index as parameters, not any world position
	void FaceFront(int ix, int iy, int iz, glm::vec2 uv);
	void FaceBack(int ix, int iy, int iz, glm::vec2 uv);
	void FaceLeft(int ix, int iy, int iz, glm::vec2 uv);
	void FaceRight(int ix, int iy, int iz, glm::vec2 uv);
	void FaceUp(int ix, int iy, int iz, glm::vec2 uv);
	void FaceDown(int ix, int iy, int iz, glm::vec2 uv);
};

