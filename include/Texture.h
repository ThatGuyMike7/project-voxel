#pragma once

#include <glad/glad.h>
#include <string>

typedef unsigned char BYTE;

/*
	Loads a texture from file and generates an OpenGL texture object to be used for rendering.
	Destructor unloads the OpenGL texture.
*/
class Texture
{
public:
	// Specify the path of the texture to be loaded.
	Texture(const std::string& texturePath);
	virtual ~Texture();

	Texture(const Texture& tex) = delete;
	Texture& operator=(const Texture& tex) = delete;

	// Load the texture from file, let OpenGL auto-generate mipmaps if requested and upload it to the GPU. Returns false if an error occurs.
	bool load(bool clamp, bool filter, bool genMipmaps);

	inline int getWidth() const { return width; }
	inline int getHeight() const { return height; }

	// Returns path to the texture file on disk
	const std::string& getPath() const;

	unsigned int getTexture() const;

protected:

	// Called after the texture is generated and bound, and before it is uploaded to the GPU.
	// This function should be used for passing texture parameters to OpenGL.
	virtual void configure(BYTE* data, GLint wrapMode, GLint minFilter, GLint maxFilter);

	// Is called at the end of the load() method, if loading was successful. Note that the texture object is not bound anymore here.
	virtual void postLoad() {}

	// Size of the texture in pixels
	int width, height;

	// Path to the texture file on disk
	std::string path;

	// OpenGL texture ID
	unsigned int texture;
};
