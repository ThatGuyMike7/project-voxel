#pragma once

#include "Camera.h"
#include "DeltaTime.h"

#include <glm/glm.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>

// Camera that includes functionality for moving freely in space using WASD and the mouse
class FreeCamera : public Camera
{
public:
	FreeCamera(Window* window, DeltaTime* time, float moveSpeed = 8.0f, float fov = 45.0f, float nearPlane = 0.1f, float farPlane = 100.0f, glm::vec3 position = glm::vec3(0.0f, 0.0f, 10.0f));
	virtual ~FreeCamera();

	// Processes keyboard & mouse input to calculate view & projection matrices for a freeform FPS camera
	void update() override;

	// How many units the camera should be able to move per second
	float moveSpeed;

private:

	// Euler angles
	float yaw, pitch;

	DeltaTime* time;
};

