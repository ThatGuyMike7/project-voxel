#pragma once

#include "Texture.h"

#include <string>
#include <glm/vec2.hpp>

typedef unsigned char BYTE;

class Tileset : public Texture
{
public:
	// Specify the path of the tileset texture to load, as well as the resolution of a single tile inside the tileset in pixels
	Tileset(const std::string& texturePath, int resolution);
	virtual ~Tileset();

	// Multiply the (x,y)-index of a tile in the tileset with the UV step to get the correct UV coordinates (origin in bottom-left)
	inline glm::vec2 getUVStep() const { return uvStep; }

	// Half pixel in UV coords for a top-left vertex
	inline glm::vec2 getHalfPixelTL() const { return halfPixelTL; }

	// Half pixel in UV coords for a top-right vertex
	inline glm::vec2 getHalfPixelTR() const { return halfPixelTR; }

	// Half pixel in UV coords for a bottom-left vertex
	inline glm::vec2 getHalfPixelBL() const { return halfPixelBL; }

	// Half pixel in UV coords for a bottom-right vertex
	inline glm::vec2 getHalfPixelBR() const { return halfPixelBR; }

protected:

	// Generate and upload custom mipmaps to avoid pixel bleed that is caused by the texture atlas
	void configure(BYTE* data, GLint wrapMode, GLint minFilter, GLint maxFilter) override;

	// Calculate UV step with half pixel correction
	void postLoad() override;

private:

	// Generate `count` mipmaps from the base texture (`data`) and upload them to OpenGL
	void generateMipmaps(BYTE* data, unsigned int count);

	// Resolution of a single tile inside the tileset in pixels
	int resolution;

	glm::vec2 uvStep;

	// Half pixel in UV coords for the four vertices of a tile
	glm::vec2 halfPixelTL, halfPixelTR, halfPixelBL, halfPixelBR;
};
