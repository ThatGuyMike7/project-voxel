#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

const glm::vec3 WORLD_UP = glm::vec3{ 0.0f, 1.0f, 0.0f };
const glm::vec3 WORLD_DOWN = glm::vec3{ 0.0f, -1.0f, 0.0f };
const glm::vec3 WORLD_RIGHT = glm::vec3{ 1.0f, 0.0f, 0.0f };
const glm::vec3 WORLD_LEFT = glm::vec3{ -1.0f, 0.0f, 0.0f };
const glm::vec3 WORLD_FRWD = glm::vec3{ 0.0f, 0.0f, -1.0f };
const glm::vec3 WORLD_BACK = glm::vec3{ 0.0f, 0.0f, 1.0f };

const glm::vec3 VEC_ZERO = glm::vec3{ 0.0f, 0.0f, 0.0f };
const glm::vec3 VEC_ONE = glm::vec3{ 1.0f, 1.0f, 1.0f };

const glm::quat QUAT_IDENTITY = glm::quat{ 1.0f, 0.0f, 0.0f, 0.0f };
const glm::mat4 MAT_IDENTITY = glm::mat4{ 1.0f };

// Constructs a transformation matrix from position, rotation and scale.
class Transform
{
public:
	// Initialize the transform with a zero-vector position, identity orientation and one-vector scale.
	Transform();

	// Initialize the transform with a position, orientation and scale.
	Transform(const glm::vec3& position,
		      const glm::quat& orientation = QUAT_IDENTITY,
			  const glm::vec3& scale = VEC_ONE);

	// Initialize the transform with a position, euler angles and scale.
	Transform(const glm::vec3& position,
			  const glm::vec3& eulerAngles = VEC_ZERO,
			  const glm::vec3& scale = VEC_ONE);

	//Transform(const Transform& trans) = default;
	//Transform& operator=(const Transform& trans) = default;

	~Transform();

	// Calculates and returns the transformation matrix, which is the position, orientation and scale combined
	glm::mat4 getModel();

	// Translate on the world axes
	//void translate(const glm::vec3& amount);

	// Translate on the local axes
	//void translateLocal(const glm::vec3& amount);

	// Rotate around an axis by 'angle' degrees
	void rotateAround(const glm::vec3& axis, float angle);

	// Set the orientation as euler angles in degrees
	void setEulerAngles(const glm::vec3& eulerAngles);

	// Get the transform's forward vector
	glm::vec3 forward() const;

	// Get the transform's right vector
	glm::vec3 right() const;

	// Get the transform's up vector
	glm::vec3 up() const;

	glm::vec3 position;
	glm::quat orientation;
	glm::vec3 scale;

private:

};

