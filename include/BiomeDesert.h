#pragma once

#include "Biome.h"

#include <noise/noise.h>

namespace gen
{
	namespace biome
	{
		// Grassland plains biome
		class Desert : public Biome
		{
		public:
			Desert(Seed* seed);
			virtual ~Desert();

			double getValue(const NoiseCoords& coords) override;

		private:
			noise::module::Perlin perlinMap;
			noise::module::Clamp clampMod;
		};
	}
}