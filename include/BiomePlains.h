#pragma once

#include "Biome.h"

#include <noise/noise.h>

namespace gen
{
	namespace biome
	{
		// Grassland plains biome
		class Plains : public Biome
		{
		public:
			Plains(Seed* seed);
			virtual ~Plains();

			double getValue(const NoiseCoords& coords) override;

		private:
			noise::module::Perlin perlinMap;
			noise::module::Clamp clampMod;
		};
	}
}