#pragma once

#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>

// Keyboard and joystick input for a specific Window
class Input
{
public:
	// Default constructor with no window specified.
	Input() { }
	// Set which GLFW window is taken input from and the mouse sensitivity
	Input(GLFWwindow* window, float mouseSensitivityX = 0.0014f, float mouseSensitivityY = 0.0014f);

	Input(const Input& input) = delete;
	Input& operator=(const Input& input) = delete;

	/// Mouse
	// Sensitivity factor applied to raw delta for the final mouse delta
	float sensitivityX, sensitivityY;

	// Returns mouse movement with applied sensitivity (-y is up)
	glm::vec2 getMouseDelta();

	// Returns cursor position relative to the window
	glm::vec2 getMousePosition();

	// Returns whether or not the mouse was moved last frame
	bool isMouseMoving();
	///

	/// Keyboard
	bool getKeyDown(int key);
	bool getKeyUp(int key);
	///

	// Called by Window before GLFW events are polled
	void prePoll();

	// Called by Window after GLFW events are polled
	void postPoll();

	// GLFW event forwarded by the Window class
	virtual void onMouseMove(float x, float y);

private:

	// Raw mouse delta * sensitivity
	float mouseDeltaX, mouseDeltaY;

	// Current position of the cursor
	float mouseX, mouseY;

	// Whether or not the mouse has been moved this frame
	bool mouseMoving;

	// First mouse callback received?
	bool firstMouse;

	// Non-ownership pointer to a GLFWwindow inside a Window class for processing input
	GLFWwindow* window;
};
