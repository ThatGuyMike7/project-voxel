#pragma once

#include <vector>
#include <map>
#include <forward_list>
#include <algorithm> // for std::swap
#include <optional>
//#include <exception>


// Contiguous block of memory (vector) with fast lookup (map) and stable indices (forward_list)
template <class key, class value>
class SlotMap
{
public:
	
	typedef typename std::vector<std::optional<value>>::iterator SlotMapIterator;

	SlotMap(unsigned int internalSize);
	~SlotMap();

	SlotMap(const SlotMap& slotmap) = delete;
	SlotMap& operator=(const SlotMap& slotmap) = delete;

	SlotMapIterator begin();
	SlotMapIterator end();

	bool isEmpty() const;

	int add(const key& k, value&& toAdd);
	bool remove(const key& k);

	std::optional<value>* get(int i);
	std::optional<value>* get(const key& k);
	int getIndex(const key& k) const;
	bool exists(const key& k) const;

	// Returns where the last element is in the internal vector + 1
	inline int size() const { return last + 1; }

	// Returns the maximum size of the internal vector
	inline int getInternalSize() const { return internalSize; }

private:
	// Size of data vector
	int internalSize;
	
	// Index of last element in the slotmap
	int last;
	
	// End iterator pointing at the element after 'last'
	//SlotMapIterator dataEnd;

	std::vector<std::optional<value>> data;
	std::map<key, int> lookup;
	std::forward_list<int> freeList;
};

// Initialise the slotmap with 'count' empty slots
template <class key, class value>
SlotMap<key, value>::SlotMap(unsigned int internalSize)
	: internalSize(internalSize), data(internalSize), last(-1)//, dataEnd(data.begin() + 1)
{ }

template <class key, class value>
SlotMap<key, value>::~SlotMap()
{ }

// Add an object at the end of the slotmap and return its index. If the key already exists, -1 is returned. Note that when the object reaches the end of the internal vector,
// std::out_of_range exception is thrown.
template <class key, class value>
int SlotMap<key, value>::add(const key& k, value&& toAdd)
{
	// Index of the item to be added
	int itemIndex;

	/// If there are no free slots inside the data range (0 - 'last'), add the element at the end.
	if (freeList.empty())
	{
		itemIndex = last + 1;
		
		// If the key does not exist yet, store index of this object in lookup table
		bool emplaceSuccess = lookup.try_emplace(k, itemIndex).second;

		if (!emplaceSuccess)
			return -1; // TODO: Throw an exception

		// Note that `at()` has a bounds check and throws std::out_of_range if not in bounds.
		// std::optional move ctor leaves the moved-from optional as it was and does not empty it.
		// We have to do this manually. TODO: Do just that.
		data.at(itemIndex) = std::forward<value>(toAdd);

		// This is now the last element
		last = itemIndex;

		// Increase end iterator
		//dataEnd++;// = data.begin() + (last + 1);
	}
	/// Freelist does have indices of free slots
	else
	{
		// Retrieve a free slot
		itemIndex = freeList.front();

		// If the key does not exist yet, store index of this object in lookup table
		bool emplaceSuccess = lookup.try_emplace(k, itemIndex).second;

		if (!emplaceSuccess)
			return -1; // TODO: Throw an exception

		// std::optional move ctor leaves the moved-from optional as it was and does not empty it.
		// We have to do this manually. TODO: Do just that.
		data[itemIndex] = std::forward<value>(toAdd);

		// Remove the free slot that is now occupied
		freeList.pop_front();
	}

	return itemIndex;
}

// Remove an object from the slotmap, putting the entry on the free list.
// Returns false if the key does not exist, otherwise true.
template <class key, class value>
bool SlotMap<key, value>::remove(const key& k)
{
	auto it = lookup.find(k);

	// Stop if the to-be-removed key does not exist
	if (it == lookup.end())
		return false;
	
	int i = it->second;//lookup[k];

	// Free the element & call its destructor
	data[i] = std::nullopt;

	// Remove the key from lookup (this object is no longer existing)
	lookup.erase(k);

	// Put the index on the freelist
	freeList.push_front(i);

	return true;
}

// Retrieve an object from the slotmap by its key. Returns nullptr if no element has the specified key.
// When the object is removed and you still store a pointer to it, `std::optional` should return false when
// trying to access.
template <class key, class value>
std::optional<value>* SlotMap<key, value>::get(const key& k)
{
	auto it = lookup.find(k);

	// If an element with the desired key exists...
	if (it != lookup.end())
	{
		// Get the index of that element
		int i = it->second;//lookup.at(k);
		
		// Return a pointer to the `std::optional` element.
		// The std::optional will return false for the caller when the element has been removed from the slot map.
		return &data[i];
	}

	return nullptr;
}

// Retrieve an object from the slotmap by its index. This is faster than getting an element by key, however,
// there is no safe way of knowing whether or not the object is the same as before at that index when it is removed and a different one is added.
// The `std::optional` may or may not hold a value depending on if the slot at index `i` is empty or not.
// Note that when the index is out of range of the internal vector, std::out_of_range exception is thrown.
template <class key, class value>
std::optional<value>* SlotMap<key, value>::get(int i)
{
	return &data.at(i);
}

// Retrieve the index of an object by its key. Returns -1 if the key does not exist.
template <class key, class value>
int SlotMap<key, value>::getIndex(const key& k) const
{
	auto it = lookup.find(k);

	// If an element with the desired key exists...
	if (it != lookup.end())
	{
		// Return the index of that element
		return it->second;
	}

	// If the key does not exist, return -1
	return -1;
}

// Returns an iterator at the first element. Note that some elements might be empty
// and on the free list, thus check if the 'std::optional' returns true first before accessing the element.
template <class key, class value>
typename SlotMap<key, value>::SlotMapIterator SlotMap<key, value>::begin()
{
	return data.begin();
}

// Returns an iterator at the last element. Note that some elements might be empty
// and on the free list, thus check if the 'std::optional' returns true first before accessing the element.
template <class key, class value>
typename SlotMap<key, value>::SlotMapIterator SlotMap<key, value>::end()
{
	// Return the last object that was added, not the last in the whole data vector
	return data.begin() + (last + 1);
	//return dataEnd;
}

// Returns whether or not the slotmap is empty
template <class key, class value>
bool SlotMap<key, value>::isEmpty() const
{
	return last == -1;
}

// Returns whether or not a a key exists in the slotmap
template <class key, class value>
bool SlotMap<key, value>::exists(const key& k) const
{
	// `find()` returns the end iterator if it cannot find anything
	return lookup.find(k) != lookup.end();
}