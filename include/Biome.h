#pragma once

#include "Seed.h"
#include <string>

namespace gen
{
	// Noise coordinates that are in the correct format to be passed to `libnoise`
	struct NoiseCoords
	{
		NoiseCoords() : x(0.0), y(0.0), z(0.0)
		{ }
		NoiseCoords(double x, double y, double z) : x(x), y(y), z(z)
		{ }

		// Returns noise coordinates from a 3D block position
		static NoiseCoords fromBlock(int blockX, int blockY, int blockZ);

		// Returns noise coordinates from a 2D block position
		static NoiseCoords fromBlock(int blockX, int blockZ);

		double x;
		double y;
		double z;
	};

	// Container for temperature, humidity and elevation data
	struct Ecocline
	{
		Ecocline(double temperature, double humidity, double elevation)
			: temperature(temperature), humidity(humidity), elevation(elevation)
		{ }
		Ecocline()
			: Ecocline(0.0, 0.0, 0.0)
		{ }

		double temperature;
		double humidity;
		double elevation;
	};

	namespace biome
	{
		// Abstract overworld biome definition
		class Biome
		{
		public:
			Biome(Seed* seed, const std::string& name, const std::string& blockName, const Ecocline& ecocline);
			virtual ~Biome();

			virtual double getValue(const NoiseCoords& coords) = 0;

			inline const Ecocline& getEcocline() const { return ecocline; }

			// Returns the unique name of the biome
			inline const std::string& getName() const { return name; }

			// Returns the default block name of the biome
			inline const std::string& getBlock() const { return blockName; }

		protected:

			Ecocline ecocline;

			// Unique name of the biome
			std::string name;

			// Block used in this biome
			std::string blockName;

			Seed* seed;
		};
	}
}
