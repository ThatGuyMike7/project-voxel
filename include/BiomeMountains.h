#pragma once

#include "Biome.h"

#include <noise/noise.h>

namespace gen
{
	namespace biome
	{
		// Grassland mountains biome
		class Mountains : public Biome
		{
		public:
			Mountains(Seed* seed);
			virtual ~Mountains();

			double getValue(const NoiseCoords& coords) override;

		private:
			noise::module::Perlin perlinMap;
			noise::module::Clamp clampMod;
		};
	}
}