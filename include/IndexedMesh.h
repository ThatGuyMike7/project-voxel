#pragma once

#include "Mesh.h"
#include <vector>

struct IndexedBufferData : public BufferData
{
	IndexedBufferData() {}

	IndexedBufferData(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices) : BufferData(vertices)
	{
		this->indices = indices;
	}

	inline void clear() override { BufferData::clear(); indices.clear(); }

	std::vector<unsigned int> indices;
};

class IndexedMesh : public Mesh
{
public:
	IndexedMesh(Shader* shader);
	virtual ~IndexedMesh();

	void draw() override;

protected:
	void createBuffers(const BufferData& data) override;

	// Element Buffer Object
	unsigned int EBO;

	// How many elements there are in the EBO
	unsigned int elementCount;
};

