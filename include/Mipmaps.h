#pragma once

#include <vector>

#include <iostream> // For Debug
#include <string>

#include <stb_image_write.h>

typedef unsigned char BYTE;

int mipmap_index(int x, int y, int width)
{
	return y * width + x;
}

// Supply atlas pixel data, channels (RGB or RGBA?), atlas size, tile size, and tile position
std::vector<BYTE> mipmap_generateAtlasSubTile(BYTE* data, unsigned int numChannels, int width, int height, int tileSize, int tileX, int tileY)
{
	// Size of the data array. RGBA, so 4 channels per pixel.
	int dataSize = width * height * 4;

	// Mipmap texture data that is half the size of the source data
	int newDataSize = dataSize / 2;
	std::vector<BYTE> newData;

	// Tile size in the destination tileset
	int newTileSize = tileSize / 2;

	// Position of the tile in the destination tileset
	int startX = tileX * newTileSize;
	int startY = tileY * newTileSize;

	int endX = startX + newTileSize;
	int endY = startY + newTileSize;

	//std::cout << "startX: " << startX << "  startY: " << startY << "  endX: " << endX << "  endY: " << endY << std::endl;

	// Loop over every pixel (not channel) inside the new mipmap
	for (int y = startY; y < endY; y += 1)
	{
		for (int x = startX; x < endX; x += 1)
		{
			//int i = index(x, y, height);

			// Position of the tile in the source tileset
			int sourceX1 = x * 2;
			int sourceY1 = y * 2;
			int sourceX2 = x * 2 + 1;
			int sourceY2 = y * 2 + 1;

			// Get channels of the respective source pixels (Red, Blue, Green, and perhaps Alpha. This depends on 'numChannels'),
			// average them and add the result to the new data array:

			// For every channel of a pixel...
			for (unsigned int compCntr = 0; compCntr < numChannels; compCntr++)
			{
				// Retrieve the same component from each of the 4 pixels
				BYTE sourcePixel1Component = data[mipmap_index(sourceX1, sourceY1, width) * numChannels + compCntr];
				BYTE sourcePixel2Component = data[mipmap_index(sourceX2, sourceY2, width) * numChannels + compCntr];
				BYTE sourcePixel3Component = data[mipmap_index(sourceX1, sourceY2, width) * numChannels + compCntr];
				BYTE sourcePixel4Component = data[mipmap_index(sourceX2, sourceY1, width) * numChannels + compCntr];

				// Average the components
				BYTE destComponent = static_cast<BYTE>((static_cast<int>(sourcePixel1Component) + static_cast<int>(sourcePixel2Component) + static_cast<int>(sourcePixel3Component)
					+ static_cast<int>(sourcePixel4Component)) / 4);

				//std::cout << static_cast<int>(sourcePixel1Component) << ", " << static_cast<int>(sourcePixel2Component) << ", "
				//	<< static_cast<int>(sourcePixel3Component) << ", " << static_cast<int>(sourcePixel4Component) << ", " << static_cast<int>(destComponent) << " || ";

				// Add the average to the new data array
				newData.emplace_back(destComponent);
			}
		}
	}

	return newData;

	/*for (int i = 0; i < newData.size(); i++)
	{
		std::cout << static_cast<int>(newData[i]) << ", ";
	}*/

	//std::string filename = "_Generated_Mipmaps/mipmap" + std::to_string(rand()) + ".bmp";

	//if (!stbi_write_png(filename.c_str(), newTileSize, newTileSize, numChannels, newData.data(), 0))
	//	std::cout << "Mipmap Error: Could not save mipmap to file!" << std::endl;
}

// Supply pixel data, channels (RGB or RGBA?), texture size, and tile size of the texture atlas.
// This function downsamples an atlas texture to half the size without edge artifacts and saves the mipmap on disk as ".png".
// Returns a pointer that must be deleted manually with 'delete[]', or there is a memory leak.
BYTE* mipmap_generateAtlas(BYTE* data, unsigned int numChannels, int width, int height, int tileSize, const std::string& filename)
{
	std::vector<std::vector<BYTE>> tileMipmaps;

	// How many tiles there are in the texture atlas
	int tilesOnX = width / tileSize;
	int tilesOnY = height / tileSize;

	// Iterate over all tiles in the tileset
	for (int tileY = 0; tileY < tilesOnY; tileY++)
	{
		for (int tileX = 0; tileX < tilesOnX; tileX++)
		{
			// Generate mipmap for each tile seperately
			std::vector<BYTE> tileMipmap = mipmap_generateAtlasSubTile(data, numChannels, width, height, tileSize, tileX, tileY);
			tileMipmaps.emplace_back(tileMipmap);
		}
	}

	// Downsampled tileset pixel data
	//std::vector<BYTE> newData;

	// Size of a tile inside the downsampled tileset
	int newTileSize = tileSize / 2;

	// New size of the downsampled tileset, which is half the size of the source tileset
	int newWidth = width / 2;
	int newHeight = height / 2;

	// Downsampled tileset pixel data
	BYTE* newData = new BYTE[newWidth * newHeight * numChannels];

	// Go over every row
	for (int y = 0; y < newHeight; y++)
	{
		for (int x = 0; x < newWidth; x++)
		{
			int tileX = x / newTileSize;
			int tileY = y / newTileSize;

			// Index of the current tile in tile container
			int tileIndex = mipmap_index(tileX, tileY, tilesOnX);

			int tilePixelX = x % newTileSize;
			int tilePixelY = y % newTileSize;

			// Index of the current pixel of the current tile
			int tilePixelIndex = mipmap_index(tilePixelX, tilePixelY, newTileSize);

			// For every channel of the pixel...
			for (unsigned int compCntr = 0; compCntr < numChannels; compCntr++)
			{
				BYTE channel = tileMipmaps[tileIndex][tilePixelIndex * numChannels + compCntr];

				int i = mipmap_index(x, y, newWidth) * numChannels + compCntr;
				newData[i] = channel;
				//newData.emplace_back(channel);
			}
		}
	}

	std::string path = "res/mipmaps/" + filename + ".png";

	if (!stbi_write_png(path.c_str(), newWidth, newHeight, numChannels, newData, 0))
		std::cout << "Mipmap Error: Could not save mipmap to file!" << std::endl;

	return newData;
}
