#pragma once

#include "Shader.h"
#include "Tileset.h"
#include "Camera.h"
#include "SlotMap.h"
#include "Chunk.h"

#include "WorldGenerator.h"
#include "Seed.h"

#include <noise/noise.h>

// Representation of a world that manages chunks
class World
{
	friend class Chunk;

public:
	World(Shader* chunkShader, Tileset* chunkTileset, Camera* camera);
	~World();

	// Update the chunk shader with the camera's position for lighting calculation
	// and draw all currently loaded chunks.
	void drawChunks();

	// Returns how many chunks are in the frustum
	unsigned int drawChunksInFrustum();

	// Generate and spawn a chunk in the world and return its index in the slotmap. Note that this rebuilds the neighbour chunks.
	int loadChunk(const ChunkKey& key, bool build = true);

	// Generate a block of chunks in one pass, then generate the meshes. This is useful for the initial spawning / start of the game.
	void preload(int x, int y, int z, int width, int height, int depth);

	// Set the ambient color of the chunk shader
	void setAmbient(const glm::vec3& color, float strength);

	// Returns the world generator
	inline gen::WorldGenerator& getGenerator() { return generator; }

private:

	gen::Seed seed;
	gen::WorldGenerator generator;

	SlotMap<ChunkKey, Chunk> chunks;
	
	Shader* chunkShader;
	Tileset* chunkTileset;
	Camera* camera;
};

