#pragma once

#include "Texture.h"
#include <string>
#include <glm/glm.hpp>

// Loads vertex & fragment shader objects and frees them in the destructor.
class Shader
{
public:
	Shader(Texture* texture, const std::string& vertexShaderPath, const std::string& fragmentShaderPath);
	virtual ~Shader();

	Shader(const Shader& shader) = delete;
	Shader& operator=(const Shader& shader) = delete;

	// Read and compile the vertex and fragment shader objects. Returns false if there is an error.
	bool compile();

	// Set the shader's sampler(s) for any textures to be used. Must be called after compile().
	virtual void initialize();

	// Binds to the shader and its texture unit(s)
	virtual void use();

	// Returns the compiled shader program
	unsigned int getProgram() const;

	// Returns the shader's texture
	Texture* getTexture() const;

	// Change the texture used for rendering this mesh. Returns false if the passed texture is nullptr.
	bool setTexture(Texture* texture);

	/// Uniforms
	void setUniformVec3(const std::string& name, const glm::vec3& value) const;
	void setUniformMat4(const std::string& name, const glm::mat4& value) const;
	void setUniformb(const std::string& name, bool value) const;
	void setUniformi(const std::string &name, int value) const;
	void setUniformf(const std::string &name, float value) const;
	///

	std::string getVertexShaderPath() const;
	std::string getFragmentShaderPath() const;

protected:

	// Which texture to supply to the shader
	Texture* texture;

	unsigned int shaderProgram;

	std::string vertexShaderPath;
	std::string fragmentShaderPath;
};

