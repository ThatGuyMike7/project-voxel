#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "Window.h"
#include "Transform.h"

class Camera
{
	friend class Window;

public:
	// Assign the window this camera is used in. Must not be nullptr!
	Camera(Window* window,
		  float fov = 45.0f, float nearPlane = 0.1f, float farPlane = 100.0f, const glm::vec3& position = glm::vec3(0.0f, 0.0f, 10.0f));

	virtual ~Camera();

	Camera(const Camera& cam) = delete;
	Camera& operator=(const Camera& cam) = delete;

	// Recalculates the view and projection matrices. Should be called before any points are tested against the frustum.
	virtual void update();

	// Test if a point in world coordinates is inside the view frustum
	bool frustumTestPoint(const glm::vec3& point);

	// Near clipping plane
	float nearPlane;

	// Far clipping plane
	float farPlane;

	// Returns the camera's field of view in degrees
	inline float getFov() const { return fov; }

	// Set the camera's field of view
	void setFov(float fov);

	glm::mat4 view;
	glm::mat4 projection;

	Transform transform;

protected:

	// Field of view in radians
	float fov;
	float fovTan;

	float nearPlaneWidth;
	float nearPlaneHeight;

	glm::vec3 frustumX, frustumY, frustumZ;

	Window* window;

private:

	// Event called by `Window` whenever the viewport changes
	void onResize();
};
