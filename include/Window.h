#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <memory>

#include "Input.h"
#include "DeltaTime.h"

class Camera;

// GLFWwindow and GLAD wrapper. glfwTerminate() is called automatically in the destructor
class Window
{
public:

	Window(bool limitFramerate = true, unsigned int targetFramerate = 100u);
	~Window();

	Window(const Window& wnd) = delete;
	Window& operator=(const Window& wnd) = delete;

	// Open the window with an OpenGL context and load OpenGL functions using GLAD. Returns true if successful
	bool open(int width, int height, const char* title, int MSAASampleCount = 0);

	// Polls events, updates input and limits framerate if requested.
	// Run this as a do-while-loop condition and execute non-limited code in it, while
	// doing rendering outside of it, which will result in the rendering being limited to
	// the 'targetFramerate'. When 'limitFramerate' is disabled, this method always returns false.
	// Pass a delta time to this method that gets recalculated every tick (or: every do-while loop step).
	bool update(float deltaTime);

	// Measure the framerate by counting frames for 1 second and average the results.
	// Call this in the rendering code if you want access to the framerate value.
	// It uses its own instance of the 'DeltaTime' class which is recalculated in this method
	// in order to retrieve the time passed since the last render call, instead of the last
	// tick call.
	void measureFramerate();

	// Start the Dear ImGui frame
	void startUIFrame() const;

	// Draw ImGui frame
	void endUIFrame() const;

	// Draw frame
	void swapBuffers() const;

	// Set close flag to true
	void close() const;

	// Whether or not the window should be open (close flag false)
	bool isOpen() const;

	inline Input& getInput() const { return *input; }

	// Returns the width/height ratio
	inline float getRatio() const { return ratio; }

	// Returns the camera that is updated when the window's viewport changes
	inline Camera* getCamera() const { return camera; }

	// Set the camera that should be updated whenever the viewport changes
	void setCamera(Camera* camera);

	// Returns the width and height of the window's framebuffer
	inline glm::ivec2 getFramebufferSize() const { return framebufferSize; }

	// Resets the framerate timer. Should be called after loading is done
	inline void reset() { renderTime.reset(); }

	// Returns the average of the last 5 framerate measurements (last 5 seconds)
	inline unsigned int getFramerate() { return averageFramerate; }

	inline unsigned int getTargetFramerate() { return targetFramerate; }
	inline void setTargetFramerate(unsigned int targetFramerate) { this->targetFramerate = targetFramerate; this->targetPeriod = 1.0f / static_cast<float>(targetFramerate); }

	/// GLFW Events
	virtual void onResize(int width, int height);
	virtual void onMouseMove(float x, float y);
	///

private:

	// Timer for determining how long to block rendering if 'limitFramerate' is enabled
	float limitFramerateTime = 0.0f;

	// Whether or not the framerate should be limited to 'targetFramerate'
	bool limitFramerate = false;

	// Array of FPS values from the last 'frameratesSize' measures
	unsigned int* framerates;
	unsigned int frameratesSize;

	// At which position in the array to put the next FPS measure result (goes back to 0 after reaching 'frameratesSize')
	unsigned int frameratesIndex = 0u;

	// Which FPS to target
	unsigned int targetFramerate = 0u;

	// 1.0f / targetFramerate
	float targetPeriod = 0.0f;

	// Timer that goes up to 1 second for measuring frames per second
	float fpsMeasureTimer = 0.0f;

	// Frame counter for measuring frames per second
	unsigned int frameCounter = 0u;

	// Average of all measures from the framerates array
	unsigned int averageFramerate = 0u;
	///

	/// GLFW Callbacks
	// GLFW free function window resize event
	static void framebufferSizeCallback(GLFWwindow* window, int width, int height);

	// GLFW free function mouse move event
	static void mouseMoveCallback(GLFWwindow* window, double x, double y);
	///

	// Width/height ratio
	float ratio = 0.0f;

	// Current framebuffer size set by GLFW callback
	glm::ivec2 framebufferSize;

	// Seperate 'DeltaTime' instance to retrieve the time since the last render call.
	// This is used to evaluate the framerate.
	DeltaTime renderTime;

	// Camera class that is updated whenever the viewport changes
	Camera* camera;

	// Input class is pointing to the same GLFWwindow
	std::unique_ptr<Input> input;
	GLFWwindow* window;
};

