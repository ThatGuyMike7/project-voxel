#pragma once

namespace gen
{
	// Handles getting multiple seed values from a single base seed, so that if not desired, there are no noise maps that use the same seed.
	class Seed
	{
	public:
		Seed(int baseSeed)
			: baseSeed(baseSeed), nextCntr(0)
		{ }

		// Conversion to int returns base seed
		operator int() const { return baseSeed; }

		inline int getNext()
		{
			nextCntr++;
			return baseSeed + nextCntr;
		}

	private:
		int baseSeed;
		int nextCntr;
	};
}