#version 330 core

// Built-in variable
out vec4 FragColor;

// Input from the vertex shader
in vec2 texCoord;

// Texture unit. There should be a minimum of 16 available per shader.
uniform sampler2D texture0;

void main()
{
	FragColor = texture(texture0, texCoord);
	//FragColor = vec4(myColor, 1.0);
}