#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

// Output UV to fragment shader
out vec2 texCoord;

// Output normal to fragment shader
out vec3 normal;

// Fragment position in world space
out vec3 FragPos;

// Transformation matrices
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	// Note that we read the multiplication from right to left
	gl_Position = projection * view * model * vec4(aPos, 1.0);
	FragPos = vec3(model * vec4(aPos, 1.0));
	
	texCoord = aTexCoord;
	
	// TODO: Transform normal into world space
	// This is a costly operation and should be calculated on the CPU and sent to the shader via a uniform
	
	//mat3 Normal = mat3(transpose(inverse(model))) * aNormal;
	
	
	normal = aNormal;
}