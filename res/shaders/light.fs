#version 330 core

// Built-in variable
out vec4 FragColor;

// Input from the vertex shader
in vec2 texCoord;
in vec3 normal;
in vec3 FragPos;

// Texture unit. There should be a minimum of 16 available per shader.
uniform sampler2D texture0;

uniform vec3 ambientColor;
uniform float ambientStrength;

// Should be the camera
uniform vec3 lightPos;

void main()
{
	// Constant light color (for now)
	vec3 lightColor = vec3(1.0, 1.0, 1.0);

	// Get ambient
	vec3 ambient = ambientStrength * ambientColor;

	// Calculate diffuse
	vec3 norm = normalize(normal);
	vec3 lightDir = normalize(lightPos - FragPos);
	float diff = clamp(dot(norm, lightDir), 0.5, 1.0); // Clamp diffuse value so it doesn't get negative, nor too dark
	
	// Get diffuse
	vec3 diffuse = diff * lightColor;
	
	// Combine ambient and diffuse
	vec4 lightResult = vec4(diffuse * ambient, 1.0);

	// Combine lighting with texture
	FragColor = texture(texture0, texCoord) * lightResult;
}