BUILDIR := build
BINDIR := bin/x64
TARGET := ProjectVoxel

.PHONY: all
all: build

.PHONY: build
build:
	mkdir build ; cd build ; cmake -G "Unix Makefiles" -D CMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++-8 .. ; \
	cmake --build . --config Release

.PHONE: run
run: build
	@echo "\tRunning application..."
	@${BINDIR}/${TARGET}

.PHONY: clean
clean:
	@echo "\tRemoving binary files"
	@rm -rf bin

.PHONY: cleanall
cleanall:
	@echo "\tRemoving binary files and whatever CMake built"
	@rm -rf bin
	@rm -rf ${BUILDIR}
