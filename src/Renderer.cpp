#include "Renderer.h"

#include "Shader.h"

#include <glad/glad.h>
#include <iostream>

Renderer::Renderer(Camera* camera)
	: camera(camera)
{ }

Renderer::~Renderer()
{ }

Mesh& Renderer::moveMesh(Mesh&& mesh)
{
	return meshList.emplace_back(std::move(mesh));
}

void Renderer::bruteDraw(Shader* shader)
{
	// Bind to the shader and its texture units
	shader->use();

	// Supply view & projection only ONCE per frame, not once per object!
	shader->setUniformMat4("view", camera->view);
	shader->setUniformMat4("projection", camera->projection);

	for (unsigned int i = 0; i < meshList.size(); i++)
	{
		//std::cout << i << std::endl;
		Mesh* mesh = &meshList[i];

		//mesh->transform.rotateAround(WORLD_UP, 5.0f);
		//mesh->transform.rotateAround(WORLD_RIGHT, 2.5f);

		// Supply mesh transformation
		shader->setUniformMat4("model", mesh->transform.getModel());

		mesh->draw();
	}
}