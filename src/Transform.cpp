#include "Transform.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>

#include <iostream>

Transform::Transform()
	: Transform(VEC_ZERO, QUAT_IDENTITY, VEC_ONE)
{
}

Transform::Transform(const glm::vec3& position, const glm::vec3& eulerAngles, const glm::vec3& scale)
	: Transform(position,
		glm::yawPitchRoll(glm::radians(eulerAngles.y), glm::radians(eulerAngles.x), glm::radians(eulerAngles.z)),
		scale)
{
}

Transform::Transform(const glm::vec3& position, const glm::quat& orientation, const glm::vec3& scale)
{
	this->position = position;
	this->orientation = orientation;
	this->scale = scale;
}

Transform::~Transform()
{
	
}

glm::mat4 Transform::getModel()
{
	// Translation * Rotation * Scale
	glm::mat4 model = MAT_IDENTITY;
	model = glm::translate(model, position) * glm::mat4_cast(orientation) * glm::scale(model, scale);
	//model = glm::scale(model, scale);

	return model;
}

void Transform::rotateAround(const glm::vec3& axis, float angle)
{
	orientation *= glm::angleAxis(glm::radians(angle), axis /* * orientation */);
}

void Transform::setEulerAngles(const glm::vec3& eulerAngles)
{
	orientation = glm::yawPitchRoll(glm::radians(eulerAngles.y), glm::radians(eulerAngles.x), glm::radians(eulerAngles.z));
}

glm::vec3 Transform::forward() const
{
	//glm::vec4 f = glm::inverse(orientation) * glm::vec4(0, 0, -1, 1);
	glm::vec4 f = orientation * glm::vec4(0, 0, -1, 1);
	return glm::vec3(f);
}

glm::vec3 Transform::right() const
{
	//glm::vec4 r = glm::inverse(orientation) * glm::vec4(1, 0, 0, 1);
	glm::vec4 r = orientation * glm::vec4(1, 0, 0, 1);
	return glm::vec3(r);
}

glm::vec3 Transform::up() const
{
	//glm::vec4 u = glm::inverse(orientation) * glm::vec4(0, 1, 0, 1);
	glm::vec4 u = orientation * glm::vec4(0, 1, 0, 1);
	return glm::vec3(u);
}