#include "Shader.h"

#include <glad/glad.h>
#include <fstream>
#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

Shader::Shader(Texture* texture, const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	// nullptr check is inside 'setTexture'
	setTexture(texture);

	this->vertexShaderPath = vertexShaderPath;
	this->fragmentShaderPath = fragmentShaderPath;
}

Shader::~Shader()
{
	glDeleteProgram(shaderProgram);
}

unsigned int Shader::getProgram() const
{
	return shaderProgram;
}

bool Shader::compile()
{
	// Check if vertex & fragment shader files are accessable 
	std::ifstream inVertexFile{ vertexShaderPath };
	if (!inVertexFile.good())
		return false;

	std::ifstream inFragmentFile{ fragmentShaderPath };
	if (!inFragmentFile.good())
		return false;

	// Read the contents of both files
	std::string vertexShaderSourceStr { std::istreambuf_iterator<char>(inVertexFile), {} };
	std::string fragmentShaderSourceStr{ std::istreambuf_iterator<char>(inFragmentFile), {} };

	const char* vertexShaderSource = vertexShaderSourceStr.c_str();
	const char* fragmentShaderSource = fragmentShaderSourceStr.c_str();

	// Compile the vertex shader
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
	glCompileShader(vertexShader);

	// Compile the fragment shader
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);
	glCompileShader(fragmentShader);

	// Check if any errors were raised during compilation...
	int success;
	char infoLog[512];

	// Of the vertex shader...
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, nullptr, infoLog);
		std::cout << "Shader Error: Compilation of Vertex Shader '" << vertexShaderPath << "' failed!" << std::endl << infoLog << std::endl;
		return false;
	}

	// And fragment shader...
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, nullptr, infoLog);
		std::cout << "Shader Error: Compilation of Fragment Shader '" << fragmentShaderPath << "' failed!" << std::endl << infoLog << std::endl;
		return false;
	}

	// Link both vertex & fragment shader objects together
	shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	// Check if linking raised any errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, nullptr, infoLog);
		std::cout << "Shader Error: Linking of Vertex and Fragment Shaders failed!" << std::endl << infoLog << std::endl;
		return false;
	}
	
	// Once linked, we can delete the vertex & fragment shader objects
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return true;
}

void Shader::initialize()
{
	glUseProgram(shaderProgram);

	// Tell the shader that its sampler 'texture0' corresponds to the Texture Unit 0 (can be activated using glActiveTexture()).
	setUniformi("texture0", 0);

	// How to add more textures to a shader:
	//shader->setInt("texture1", 1);

	// And how to bind them in use():
	//glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, texture2->getTexture());
}

void Shader::use()
{
	glUseProgram(shaderProgram);

	// For now, only 1 texture unit (multiple textures can be used, e.g. normal map, light map etc.).
	// The call to glBindTexture() after glActiveTexture(GL_TEXTURE0) binds the texture to the texture unit 0, which corresponds to the sampler2D `texture0` in the shader.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->getTexture());	
}

Texture* Shader::getTexture() const
{
	return texture;
}

bool Shader::setTexture(Texture* texture)
{
	if (texture == nullptr)
	{
		std::cout << "Shader Error: Invalid Texture selected!" << std::endl;
		return false;
	}

	this->texture = texture;
	return true;
}

void Shader::setUniformVec3(const std::string& name, const glm::vec3& value) const
{
	glUniform3fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void Shader::setUniformMat4(const std::string& name, const glm::mat4& value) const
{
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
}

void Shader::setUniformb(const std::string& name, bool value) const
{
	glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), static_cast<int>(value));
}

void Shader::setUniformi(const std::string &name, int value) const
{
	glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), value);
}

void Shader::setUniformf(const std::string &name, float value) const
{
	glUniform1f(glGetUniformLocation(shaderProgram, name.c_str()), value);
}

std::string Shader::getVertexShaderPath() const
{
	return vertexShaderPath;
}

std::string Shader::getFragmentShaderPath() const
{
	return fragmentShaderPath;
}