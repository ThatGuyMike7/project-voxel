#include "BiomePlains.h"

gen::biome::Plains::Plains(Seed* seed)
	: Biome(seed, "Plains", "Grass",
		Ecocline{ 0.0, 0.0, 0.0 })
{
	perlinMap.SetSeed(seed->getNext());
	perlinMap.SetOctaveCount(4);
	perlinMap.SetFrequency(0.12);
	perlinMap.SetPersistence(0.55);
	
	clampMod.SetSourceModule(0, perlinMap);
	clampMod.SetBounds(-0.7, -0.3);
}

gen::biome::Plains::~Plains()
{
}

double gen::biome::Plains::getValue(const NoiseCoords& coords)
{
	return clampMod.GetValue(coords.x, coords.y, coords.z);
}