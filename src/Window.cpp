#include "Window.h"
#include <iostream>

// std::this_thread::sleep_for(std::chrono::milliseconds(x));
#include <chrono>
#include <thread>

#include "Camera.h"

#include <imgui.h>
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

Window::Window(bool limitFramerate, unsigned int targetFramerate)
	: limitFramerate(limitFramerate)
{
	setTargetFramerate(targetFramerate);

	// Store a maximum of 5 framerate measurements (of the past 5 seconds)
	this->frameratesSize = 5u;
	this->framerates = new unsigned int[frameratesSize];

	// Fill framerates array with start values
	for (unsigned int i = 0u; i < frameratesSize; i++)
	{
		framerates[i] = targetFramerate;
	}
}

Window::~Window()
{
	delete[] framerates;

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwTerminate();
}

void Window::measureFramerate()
{
	renderTime.calc();

	// Calculate frames per second. Uses render-deltatime that gets called every render step
	fpsMeasureTimer += renderTime.getDelta();
	if (fpsMeasureTimer > 1.0f)
	{
		//std::cout << "Average FPS: " << averageFramerate << " Current FPS: " << frameCounter << std::endl;
		framerates[frameratesIndex] = frameCounter;

		// Put next measure result at next position in the array.
		// If the index exceeds the array size, go back to 0.
		frameratesIndex++;
		if (frameratesIndex == frameratesSize)
			frameratesIndex = 0u;

		fpsMeasureTimer -= 1.0f;
		frameCounter = 0u;

		/// Calculate the average of the last 5 framerate measurements
		// Average fps (from last 'frameratesSize' measures)
		averageFramerate = 0u;
		for (unsigned int i = 0u; i < frameratesSize; i++)
		{
			averageFramerate += framerates[i];
		}
		averageFramerate /= frameratesSize;
	}
	else
	{
		frameCounter++;
	}
}

bool Window::update(float deltaTime)
{
	// Objective: Return false when limit, true when do render call

	// Poll events
	input->prePoll();
	glfwPollEvents();
	input->postPoll();

	// Limit the framerate if desired
	if (limitFramerate)
	{
		limitFramerateTime += deltaTime;

		// Blocked enough time? Return false for a render call
		if (limitFramerateTime > targetPeriod)
		{
			limitFramerateTime -= targetPeriod;
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		// Don't block at all
		return false;
	}
}

bool Window::open(int width, int height, const char* title, int MSAASampleCount)
{
	// Specify OpenGL version used (OpenGL 3.3 Core)
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Enable higher versions of OpenGL
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Enable / disable MSAA. Can only be set at start
	glfwWindowHint(GLFW_SAMPLES, MSAASampleCount);

	// Create window with OpenGL Context
	window = glfwCreateWindow(width, height, title, nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Window Error: Failed to create GLFW window!" << std::endl;
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(window);

	// Load OpenGL functions with GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Window Error: Failed to initialize GLAD!" << std::endl;
		glfwTerminate();
		return false;
	}

	// Bind this class instance to GLFW's user pointer for retrieval in callback functions
	glfwSetWindowUserPointer(window, this);

	// Set up window resize callback
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// Set up mouse move callback
	glfwSetCursorPosCallback(window, mouseMoveCallback);

	// Put window in center position
	const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(window, vidmode->width / 2 - width / 2, vidmode->height / 2 - height / 2);

	// Set clear color
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

	// Hide and restrict cursor to window
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // TODO: Change this due to GLFW lagg https://github.com/glfw/glfw/issues/691

	// Disable VSync
	glfwSwapInterval(0);

	// Store initial framebuffer size
	//int fbw, fbh;
	//glfwGetFramebufferSize(window, &fbw, &fbh);
	//this->framebufferSize = glm::ivec2{ fbw, fbh };
	this->framebufferSize = glm::ivec2{ width, height };

	// Calculate width/height ratio
	ratio = static_cast<float>(width) / static_cast<float>(height);

	// Create Input class instance
	input = std::make_unique<Input>(window);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Cull backfaces
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Enable MSAA
	glEnable(GL_MULTISAMPLE);

	// Set up Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init();

	// Setup Style
	ImGui::StyleColorsDark();

	return true;
}

bool Window::isOpen() const
{
	if (glfwWindowShouldClose(window))
		return false;

	return true;
}

void Window::close() const
{
	glfwSetWindowShouldClose(window, true);
}

void Window::startUIFrame() const
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void Window::endUIFrame() const
{
	ImGui::Render();
	//glfwMakeContextCurrent(window);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	//glfwMakeContextCurrent(window);
}

void Window::swapBuffers() const
{
	glfwSwapBuffers(window);
}

void Window::setCamera(Camera* camera)
{
	camera->onResize();
	this->camera = camera;
}

void Window::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	Window* w = static_cast<Window*>(glfwGetWindowUserPointer(window));
	w->onResize(width, height);
}

void Window::mouseMoveCallback(GLFWwindow* window, double x, double y)
{
	Window* w = static_cast<Window*>(glfwGetWindowUserPointer(window));
	w->onMouseMove(static_cast<float>(x), static_cast<float>(y));
}

void Window::onResize(int width, int height)
{
	glViewport(0, 0, width, height);
	framebufferSize = glm::ivec2{ width, height };

	// Recalculate width/height ratio
	ratio = static_cast<float>(width) / static_cast<float>(height);

	// Notify camera of viewport change
	camera->onResize();
}

void Window::onMouseMove(float x, float y)
{
	// Forward the event to the Input class
	input->onMouseMove(x, y);
}
