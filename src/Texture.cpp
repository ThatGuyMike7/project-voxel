#include "Texture.h"

#include <stb_image.h>
#include <iostream>
#include <exception>

Texture::Texture(const std::string& texturePath)
{
	this->path = texturePath;
}

Texture::~Texture()
{
	glDeleteTextures(1, &texture);
}

bool Texture::load(bool clamp, bool filter, bool genMipmaps)
{
	try
	{
		// Load the texture from disk
		int numChannels;
		BYTE* texData = stbi_load(path.c_str(), &this->width, &this->height, &numChannels, STBI_rgb_alpha);

		if (texData == nullptr)
		{
			std::cout << "Texture Error: Unable to load texture using stbi_load!" << std::endl;
			return false;
		}

		GLint wrapMode = clamp ? GL_CLAMP_TO_EDGE : GL_REPEAT;
		GLint minFilter = filter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR;
		GLint maxFilter = filter ? GL_LINEAR : GL_NEAREST;

		// Generate texture using OpenGL
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		// Set texture parameters and let inherited texture classes generate custom mipmaps here if desired
		configure(texData, wrapMode, minFilter, maxFilter);

		// Specify how many mipmaps will be used
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 7);
		
		// Upload base texture to OpenGL
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texData);

		// Auto-generate mipmaps
		if(genMipmaps)
			glGenerateMipmap(GL_TEXTURE_2D);

		// Unbind from the texture
		glBindTexture(GL_TEXTURE_2D, 0);

		// Free the texture memory
		stbi_image_free(texData);
	}
	catch (const std::exception& e)
	{
		std::cout << "Texture Error: Loading texture failed!" << std::endl << e.what() << std::endl;
		return false;
	}

	postLoad();

	return true;
}

void Texture::configure(BYTE* data, GLint wrapMode, GLint minFilter, GLint maxFilter)
{
	// Set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, maxFilter);
}

unsigned int Texture::getTexture() const
{
	return texture;
}

const std::string& Texture::getPath() const
{
	return path;
}
