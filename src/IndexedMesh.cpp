#include "IndexedMesh.h"

#include <glad/glad.h>
#include <iostream>

IndexedMesh::IndexedMesh(Shader* shader) : Mesh(shader)
{
}

IndexedMesh::~IndexedMesh()
{
	glDeleteBuffers(1, &EBO);
}

void IndexedMesh::draw()
{
	shader->use();

	// Draw the mesh
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, elementCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void IndexedMesh::createBuffers(const BufferData& data)
{
	// Call base which creates Vertex Buffer Object
	Mesh::createBuffers(data);

	// Cast the bufferdata to indexed bufferdata for access to indices
	const IndexedBufferData& indexedData = static_cast<const IndexedBufferData&>(data);

	elementCount = static_cast<unsigned int>(indexedData.indices.size());

	// Create an Element Buffer Object
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	// Upload indices to OpenGL (also bound to VAO of course)
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * elementCount, indexedData.indices.data(), GL_STATIC_DRAW);
}