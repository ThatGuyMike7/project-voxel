#include "Input.h"


Input::Input(GLFWwindow* window, float mouseSensitivityX, float mouseSensitivityY)
{
	this->window = window;

	mouseX = 0.0f;
	mouseY = 0.0f;

	mouseDeltaX = 0.0f;
	mouseDeltaY = 0.0f;

	mouseMoving = false;
	firstMouse = true;

	sensitivityX = mouseSensitivityX;
	sensitivityY = mouseSensitivityY;
}

void Input::postPoll()
{

}

void Input::prePoll()
{
	// Otherwise mouseDelta would never be set to 0, since
	// the mouse callback is only called when the mouse is
	// actually moved, so it would store the delta of the last time
	// the mouse was moved, but it should be represented in real time.

	mouseDeltaX = 0.0f;
	mouseDeltaY = 0.0f;
}

void Input::onMouseMove(float x, float y)
{
	if (firstMouse)
	{
		mouseX = x;
		mouseY = y;
		firstMouse = false;
	}

	// Capture the raw mouse movement
	float rawMouseDeltaX = x - mouseX;
	float rawMouseDeltaY = y - mouseY;

	// Multiply it by the sensitivity to form the final mouse delta
	mouseDeltaX = rawMouseDeltaX * sensitivityX;
	mouseDeltaY = rawMouseDeltaY * sensitivityY;

	mouseX = x;
	mouseY = y;
}

bool Input::isMouseMoving()
{
	return getMouseDelta() != glm::vec2{ 0.0f, 0.0f };
}

glm::vec2 Input::getMouseDelta()
{
	return glm::vec2{ mouseDeltaX, mouseDeltaY };
}

glm::vec2 Input::getMousePosition()
{
	return glm::vec2{ mouseX, mouseY };
}

bool Input::getKeyDown(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool Input::getKeyUp(int key)
{
	return glfwGetKey(window, key) == GLFW_RELEASE;
}
