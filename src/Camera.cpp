#include "Camera.h"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(Window* window, float fov, float nearPlane, float farPlane, const glm::vec3& position)
	: window(window), nearPlane(nearPlane), farPlane(farPlane)
{
	setFov(fov);
	transform.position = position;

	// Pre-calculate matrices at the start
	update();
}

Camera::~Camera()
{
}

void Camera::onResize()
{
	nearPlaneHeight = nearPlane * fovTan;
	nearPlaneWidth = nearPlaneHeight * window->getRatio();
	//std::cout << "near width: " << nearPlaneWidth << " near height: " << nearPlaneHeight << " ratio: " << window->getRatio() << std::endl;
}

void Camera::setFov(float fov)
{
	this->fov = glm::radians(fov);
	fovTan = glm::tan(this->fov / 2.0f);

	//std::cout << "FOV TAN: " << fovTan << std::endl;
}

void Camera::update()
{
	/// Calculate projection matrix:
	glm::ivec2 framebufferSize = window->getFramebufferSize();
	projection = glm::perspective(fov, window->getRatio(), nearPlane, farPlane);

	/// Calculate view matrix:
	// Note that we translate the scene in the reverse direction of where the camera should move.
	// OpenGL does not know the concept of a 'camera', we simply translate the entire scene.
	//glm::vec3 cameraReverseDir = glm::normalize(position - target);
	//glm::vec3 cameraRight = glm::normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), cameraReverseDir));
	//glm::vec3 up = glm::cross(cameraReverseDir, cameraRight);

	//view = glm::lookAt(position, position + front, up);

	// Translation * Rotation * Scale
	//view = glm::inverse(glm::translate(glm::mat4{ 1.0f }, position) * glm::mat4_cast(orientation));
	view = glm::inverse(transform.getModel());

	/// Fetch frustum direction vectors:

	// Compute the Z axis of the camera referential.
	// This axis points in the same direction from the looking direction.
	frustumZ = transform.forward();//(transform.position + transform.forward()) - transform.position;//transform.right() * -1.0f - transform.position;
	frustumZ = glm::normalize(frustumZ);
	//std::cout << transform.forward().x << " " << transform.forward().y << " " << transform.forward().z << " " << std::endl;
	// X axis of camera is the cross product of Z axis and given "up" vector.
	frustumX = transform.right();//glm::cross(frustumZ, transform.up());
	frustumX = glm::normalize(frustumX);

	// The real "up" vector is the cross product of X and Z.
	frustumY = transform.up();//glm::cross(frustumX, frustumZ);
	frustumY = glm::normalize(frustumY);
}

bool Camera::frustumTestPoint(const glm::vec3& point)
{
	// Camera to point direction
	glm::vec3 dir = point - transform.position;
	float aux;

	// Compute and test the Z coordinate
	float pointCameraZ = glm::dot(dir, frustumZ);
	if (pointCameraZ < nearPlane || pointCameraZ > farPlane)
		return false;

	// Compute and test the Y coordinate
	float pointCameraY = glm::dot(dir, frustumY);
	aux = 2 * pointCameraZ * fovTan;
	if (pointCameraY < -aux || pointCameraY > aux)
		return false;

	// Compute and test the X coordinate
	float pointCameraX = glm::dot(dir, frustumX);
	aux *= window->getRatio();
	if (pointCameraX < -aux || pointCameraX > aux)
		return false;

	return true;
}
