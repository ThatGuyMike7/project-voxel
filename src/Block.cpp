#include "Block.h"

// Initialize static member
std::unordered_map<std::string, Block> Block::BLOCKS;

Block::Block() {}

Block::Block(const std::string& name, const glm::vec2& uv)
	: name(name), uv(uv)
{
}

Block::~Block()
{
}
