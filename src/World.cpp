#include "World.h"

#include <glad/glad.h>
#include <glm/vec3.hpp>
#include <vector>
#include <iostream>
#include <algorithm>

#include "Block.h"

World::World(Shader* chunkShader, Tileset* chunkTileset, Camera* camera)
	: chunkShader(chunkShader), chunkTileset(chunkTileset), camera(camera), chunks(100000), seed(58/*51525*/), generator(seed, 0.0, 128.0)
{
	setAmbient(glm::vec3{ 1.0f, 1.0f, 1.0f }, 0.7f);

	/// DEBUG

	//worldGen.writeBiomeMapRGB(5000, -2000, 1024, 1024, "biomeMapRGB.png");
	//worldGen.writeBiomeMapSectors(5000, -2000, 1024, 1024, "biomeMapSectors.png");
	//generator.writeBiomeMapHeightsAndSectors(5000, -2000, 1024, 1024, "biomeMapHeightsAndSectors.png");
	//generator.writeBiomeMapHeights(5000, -2000, 1024, 1024, "biomeMapHeights.png");

	/// END DEBUG
}

World::~World()
{
}

void World::drawChunks()
{
	/// DEBUG
	//auto weights = generator.getWeights(static_cast<int>(std::roundf(camera->transform.position.x)), static_cast<int>(std::roundf(camera->transform.position.z)));
	//std::cout << generator.biomes[weights.second]->getName() << "! " << "Pw:" << weights.first[0] << " Mw:" << weights.first[1] << " Dw:" << weights.first[2] << std::endl;
	/// END DEBUG

	// Bind to the shader and its texture units
	chunkShader->use();

	// Send camera position to chunk shader for lighting calculation
	chunkShader->setUniformVec3("lightPos", camera->transform.position);

	// Supply view & projection only ONCE per frame, not once per object!
	chunkShader->setUniformMat4("view", camera->view);
	chunkShader->setUniformMat4("projection", camera->projection);

	for (auto& it : this->chunks) // This is the fastest iteration method for `SlotMap`
	{
		if (it)
		{
			// Supply mesh transformation
			chunkShader->setUniformMat4("model", it->mesh.transform.getModel());

			// Draw the chunk mesh
			it->mesh.draw();
		}
	}
}

unsigned int World::drawChunksInFrustum()
{
	std::vector<Chunk*> visibleChunks;

	for (auto& element : this->chunks) // This is the fastest iteration method for `SlotMap`
	{
		if (element)
		{
			Chunk* current = &element.value();

			// Bounds of the chunk
			std::vector<glm::vec3> corners = current->getCorners();

			bool isVisible = false;
			std::string str;

			// Check if each corner is in the frustum
			for (unsigned int i = 0; i < corners.size(); i++)
			{
				//str += "{" + std::to_string(corners[i].x) + "," + std::to_string(corners[i].y) + "," + std::to_string(corners[i].z) + "} ";

				// If at least one corner is inside the frustum, the chunk is partially visible and thus must be rendered
				if (camera->frustumTestPoint(corners[i]))
				{
					isVisible = true;
					break;
				}
			}
			//std::cout << str << std::endl;

			if (isVisible)
				visibleChunks.emplace_back(current);
		}
	}


	// Bind to the shader and its texture units
	chunkShader->use();

	// Send camera position to chunk shader for lighting calculation
	chunkShader->setUniformVec3("lightPos", camera->transform.position);

	// Supply view & projection only ONCE per frame, not once per object!
	chunkShader->setUniformMat4("view", camera->view);
	chunkShader->setUniformMat4("projection", camera->projection);

	// Draw all the visible chunks
	for (unsigned int i = 0; i < visibleChunks.size(); i++)
	{
		Chunk* current = visibleChunks[i];

		// Supply mesh transformation
		chunkShader->setUniformMat4("model", current->mesh.transform.getModel());

		// Draw the chunk mesh
		current->mesh.draw();
	}

	return static_cast<unsigned int>(visibleChunks.size());
}

void World::setAmbient(const glm::vec3& color, float strength)
{
	glUseProgram(chunkShader->getProgram());
	chunkShader->setUniformf("ambientStrength", strength);
	chunkShader->setUniformVec3("ambientColor", color);
	glUseProgram(0);
}

int World::loadChunk(const ChunkKey& key, bool build)
{
	Chunk chunk{ key, this };

	chunk.load();

	if(build)
		chunk.build();

	chunk.mesh.transform.position = glm::vec3 {
		static_cast<float>(key.x) * CHUNK_SIZE - BLOCK_HALF,
		static_cast<float>(key.y) * CHUNK_SIZE - BLOCK_HALF,
		static_cast<float>(key.z) * CHUNK_SIZE + BLOCK_HALF
	};
	//std::cout << chunk.mesh.transform.position.x << " " << chunk.mesh.transform.position.y << " " << chunk.mesh.transform.position.z << std::endl;

	return chunks.add(key, std::move(chunk));
}

void World::preload(int x, int y, int z, int width, int height, int depth)
{
	int endX = x + width;
	int endY = y + height;
	int endZ = z + depth;

	// Store indices of generated chunks for fast access
	std::vector<int> chunkIndices;

	// Generate data only
	for (int ix = x; ix < endX; ix++)
	{
		for (int iy = y; iy < endY; iy++)
		{
			for (int iz = z; iz < endZ; iz++)
			{
				chunkIndices.emplace_back(loadChunk(ChunkKey{ ix, iy, iz }, false));
			}
		}
	}

	// Generate meshes
	for (unsigned int cntr = 0; cntr < chunkIndices.size(); cntr++)
	{
		chunks.get(chunkIndices[cntr])->value().build();
	}
}
