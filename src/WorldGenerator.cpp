#include "WorldGenerator.h"

#include "Chunk.h"

#include "BiomePlains.h"
#include "BiomeMountains.h"
#include "BiomeDesert.h"

#include <stb_image_write.h>
#include <algorithm> // for std::clamp
#include <cstdlib> // for abs()
#include <string>

#include <iostream> // for debug only


gen::WorldGenerator::WorldGenerator(Seed seed, double heightPassY, double heightScale)
	: seed(seed), heightPassY(heightPassY), heightScale(heightScale)
{
	const int BIOME_OCTAVES = 4;
	const double BIOME_FREQUENCY = 0.007;
	const double BIOME_LACUNARITY = 0.5;
	const double BIOME_PERSISTENCE = 0.25;

	// Set up biome map
	temperatureMapPerlin.SetSeed(seed);
	temperatureMapPerlin.SetOctaveCount(BIOME_OCTAVES);
	temperatureMapPerlin.SetFrequency(BIOME_FREQUENCY);
	temperatureMapPerlin.SetLacunarity(BIOME_LACUNARITY);
	temperatureMapPerlin.SetPersistence(BIOME_PERSISTENCE);

	humidityMapPerlin.SetSeed(seed.getNext());
	humidityMapPerlin.SetOctaveCount(BIOME_OCTAVES);
	humidityMapPerlin.SetFrequency(BIOME_FREQUENCY + 0.01);
	humidityMapPerlin.SetLacunarity(BIOME_LACUNARITY);
	humidityMapPerlin.SetPersistence(BIOME_PERSISTENCE);

	elevationMapPerlin.SetSeed(seed.getNext());
	elevationMapPerlin.SetOctaveCount(BIOME_OCTAVES);
	elevationMapPerlin.SetFrequency(BIOME_FREQUENCY + 0.05);
	elevationMapPerlin.SetLacunarity(BIOME_LACUNARITY);
	elevationMapPerlin.SetPersistence(BIOME_PERSISTENCE);

	// Scale the temp, hum and ele maps to make biomes bigger
	temperatureMap.SetSourceModule(0, temperatureMapPerlin);
	humidityMap.SetSourceModule(0, humidityMapPerlin);
	elevationMap.SetSourceModule(0, elevationMapPerlin);
	temperatureMap.SetScale(0.7);
	humidityMap.SetScale(0.7);
	elevationMap.SetScale(0.7);

	// Add all biomes
	biomes.emplace_back(std::make_unique<biome::Plains>(&seed));
	biomes.emplace_back(std::make_unique<biome::Mountains>(&seed));
	biomes.emplace_back(std::make_unique<biome::Desert>(&seed));
}

gen::WorldGenerator::~WorldGenerator()
{

}

gen::Ecocline gen::WorldGenerator::getEcoAtNoise(double noiseX, double noiseZ) const
{
	NoiseCoords coords{ noiseX, heightPassY, noiseZ };
	return Ecocline{ temperatureMap.GetValue(coords.x, coords.y, coords.z),
					 humidityMap.GetValue(coords.x, coords.y, coords.z),
					 elevationMap.GetValue(coords.x, coords.y, coords.z) };
}

gen::Ecocline gen::WorldGenerator::getEcoAtBlock(int blockX, int blockZ) const
{
	NoiseCoords coords = NoiseCoords::fromBlock(blockX, blockZ);
	return getEcoAtNoise(coords.x, coords.z);
}

std::pair<std::vector<double>, int> gen::WorldGenerator::getWeights(int blockX, int blockZ)
{
	NoiseCoords coords = NoiseCoords::fromBlock(blockX, blockZ);
	coords.y = heightPassY;

	// Get biome data at block position
	Ecocline blockEco = getEcoAtBlock(blockX, blockZ);

	// Weights for all the biomes
	std::vector<double> weights(biomes.size());

	// Which biome this block is a part of
	int biomeIndex = -1;

	// Highest weight determines the biome type
	double highestWeight = -5000.0;

	// Sum of all the not-normalized weights in order to normalize them after
	double sum = 0.0;

	for (unsigned int i = 0; i < biomes.size(); i++)
	{
		auto& curBiome = biomes[i];
		const Ecocline& curBiomeEco = curBiome->getEcocline();

		// Influence of the ecocline differences on biome matching
		const double TEMP_INFLUENCE = 7.0;
		const double HUM_INFLUENCE = 5.0;
		const double ELE_INFLUENCE = 5.0;

		// Calculate not-normalized weight
		double biomeWeight = square((curBiomeEco.temperature - blockEco.temperature) * TEMP_INFLUENCE)
							+ square((curBiomeEco.humidity - blockEco.humidity) * HUM_INFLUENCE)
							+ square((curBiomeEco.elevation - blockEco.elevation) * ELE_INFLUENCE);
		biomeWeight *= 0.1;
		//std::cout << biomeWeight << std::endl;

		weights[i] = biomeWeight;
		sum += biomeWeight;

		// Determine the highest weight (which is the biome of the given block)
		if (biomeWeight > highestWeight)
		{
			highestWeight = biomeWeight;
			biomeIndex = i;
		}
	}

	// Normalize the weights so they add up to 1
	double div = 1.0 / sum;
	for (unsigned int i = 0; i < biomes.size(); i++)
		weights[i] *= div;

	return std::pair<std::vector<double>, int>(weights, biomeIndex);
}

std::pair<int, int> gen::WorldGenerator::getBlockHeight(int blockX, int blockZ)
{
	// Return output of `getNoiseHeight` multiplied by `heightScale`
	std::pair<double, int> noiseHeight = getNoiseHeight(blockX, blockZ);
	return std::pair<int, int>(static_cast<int>(noiseHeight.first * heightScale), noiseHeight.second);
}

std::pair<double, int> gen::WorldGenerator::getNoiseHeight(int blockX, int blockZ)
{
	NoiseCoords coords = NoiseCoords::fromBlock(blockX, blockZ);
	coords.y = heightPassY;

	// Determine weights for all the biomes
	auto weights = getWeights(blockX, blockZ);

	// Calculate weighted average of noise outputs of all biome types, ranging from 0 to 1
	double heightNoise = 0.0;
	for (unsigned int i = 0; i < weights.first.size(); i++)
	{
		// The smaller a weight, the more it resembles the biome and thus the more it should be weighted in the
		// terrain generation process. That is why `1.0 - weight`.
		heightNoise += biomes[i]->getValue(coords) * (/*1.0 - */weights.first[i]);

		// TODO: Don't take into account weights that are > 0.9 to save processing time
	}

	// Just to be sure
	heightNoise = std::clamp<double>(heightNoise, -1.0, 1.0);

	return std::pair<double, int>(heightNoise, weights.second);
}

bool gen::WorldGenerator::writeBiomeMapHeights(int originX, int originZ, int width, int depth, const char* filename)
{
	typedef unsigned char BYTE;

	// RGB components
	const unsigned int numComponents = 3;

	bool success = true;

	// RGB data of biome map
	BYTE* data = new BYTE[width * depth * numComponents];

	int endX = originX + width;
	int endZ = originZ + depth;

	int i = 0;
	for (int z = originZ; z < endZ; z++)
	{
		for (int x = originX; x < endX; x++)
		{
			// Output is -1 to 1, so (value + 1) * 128 puts it into 0 to 255 range
			BYTE heightColor = static_cast<BYTE>(std::clamp<double>((getNoiseHeight(x, z).first + 1.0) * 128.0, 0.0, 255.0));
			//std::cout << "H=" << getHeight(x, z).first << " | Color=" << static_cast<int>(heightColor) << std::endl;

			// RGB
			data[i] = heightColor;
			i++;
			data[i] = heightColor;
			i++;
			data[i] = heightColor;
			i++;
		}
	}

	if (!stbi_write_png(filename, width, depth, numComponents, data, 0))
		success = false;

	delete[] data;
	return success;
}

bool gen::WorldGenerator::writeBiomeMapHeightsAndSectors(int originX, int originZ, int width, int depth, const char* filename)
{
	typedef unsigned char BYTE;

	// RGBA components
	const unsigned int numComponents = 4;

	bool success = true;

	// RGBA data of biome map
	BYTE* data = new BYTE[width * depth * numComponents];

	int endX = originX + width;
	int endZ = originZ + depth;

	int i = 0;
	for (int z = originZ; z < endZ; z++)
	{
		for (int x = originX; x < endX; x++)
		{
			// Height at given position and the biome
			auto heightPair = getNoiseHeight(x, z);

			// Output is -1 to 1, so (value + 1) * 128 puts it into 0 to 255 range
			BYTE heightColor = static_cast<BYTE>(std::clamp<double>((heightPair.first + 1.0) * 128.0, 0.0, 255.0));

			const std::string& biomeName = biomes[heightPair.second]->getName();

			// Plains color is red
			if (biomeName == "Plains")
			{
				// RGB
				data[i] = 100;
				i++;
				data[i] = 10;
				i++;
				data[i] = 10;
				i++;
			}
			// Mountains color is blue
			else if (biomeName == "Mountains")
			{
				// RGB
				data[i] = 10;
				i++;
				data[i] = 10;
				i++;
				data[i] = 100;
				i++;
			}
			// Desert color is green
			else if (biomeName == "Desert")
			{
				// RGB
				data[i] = 10;
				i++;
				data[i] = 100;
				i++;
				data[i] = 10;
				i++;
			}

			// Alpha
			data[i] = heightColor;
			i++;
		}
	}

	if (!stbi_write_png(filename, width, depth, numComponents, data, 0))
		success = false;

	delete[] data;
	return success;
}

bool gen::WorldGenerator::writeBiomeMapSectors(int originX, int originZ, int width, int depth, const char* filename)
{
	typedef unsigned char BYTE;

	// RGB components
	const unsigned int numComponents = 3;

	bool success = true;

	// RGB data of biome map
	BYTE* data = new BYTE[width * depth * numComponents];

	int endX = originX + width;
	int endZ = originZ + depth;

	int i = 0;
	for (int z = originZ; z < endZ; z++)
	{
		for (int x = originX; x < endX; x++)
		{
			// Determine weights for all the biomes
			auto weights = getWeights(x, z);

			bool error = false;
			if (weights.second < 0)
			{
				error = true;
				weights.second = 0;
			}

			auto& biome = biomes[weights.second];
			const std::string& biomeName = biome->getName();

			if (error)
			{
				// Error color (pink)
				data[i] = 255;
				i++;
				data[i] = 0;
				i++;
				data[i] = 255;
				i++;
				continue;
			}

			// Plains color
			if (biomeName == "Plains")
			{
				// RGB
				for (int cntr = 0; cntr < 3; cntr++)
				{
					data[i] = 255;
					i++;
				}
			}
			// Mountains color
			else if(biomeName == "Mountains")
			{
				// RGB
				for (int cntr = 0; cntr < 3; cntr++)
				{
					data[i] = 20;
					i++;
				}
			}
			// Desert color
			else if (biomeName == "Desert")
			{
				// RGB
				for (int cntr = 0; cntr < 3; cntr++)
				{
					data[i] = 100;
					i++;
				}
			}
			else
			{
				// Error color (red)
				data[i] = 255;
				i++;
				data[i] = 0;
				i++;
				data[i] = 0;
				i++;
			}
		}
	}

	if (!stbi_write_png(filename, width, depth, numComponents, data, 0))
		success = false;

	delete[] data;
	return success;
}

bool gen::WorldGenerator::writeBiomeMapRGB(int originX, int originZ, int width, int depth, const char* filename)
{
	typedef unsigned char BYTE;

	// RGB components
	const unsigned int numComponents = 3;

	bool success = true;

	// RGB data of biome map
	BYTE* data = new BYTE[width * depth * numComponents];

	int endX = originX + width;
	int endZ = originZ + depth;

	int i = 0;
	for (int z = originZ; z < endZ; z++)
	{
		for (int x = originX; x < endX; x++)
		{
			NoiseCoords coords = NoiseCoords::fromBlock(x, z);

			// Output is -1 to 1, so (value + 1) * 128 puts it into 0 to 255 range

			// R: temperature
			data[i] = static_cast<BYTE>(std::clamp<double>((temperatureMap.GetValue(coords.x, heightPassY, coords.z) + 1.0) * 128.0, 0.0, 255.0));
			i++;

			// G: humidity
			data[i] = static_cast<BYTE>(std::clamp<double>((humidityMap.GetValue(coords.x, heightPassY, coords.z) + 1.0) * 128.0, 0.0, 255.0));
			i++;

			// B: elevation
			data[i] = static_cast<BYTE>(std::clamp<double>((elevationMap.GetValue(coords.x, heightPassY, coords.z) + 1.0) * 128.0, 0.0, 255.0));
			i++;
		}
	}

	if (!stbi_write_png(filename, width, depth, numComponents, data, 0))
		success = false;

	delete[] data;
	return success;
}
