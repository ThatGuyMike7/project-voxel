#include "Mesh.h"

#include <glad/glad.h>
#include <iostream>

int Mesh::DRAW_CALLS = 0;

Mesh::Mesh() {}

Mesh::Mesh(Shader* shader) 
	: shader(shader)
{
}

Mesh::~Mesh()
{
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
}

void Mesh::build(const BufferData& data)
{
	// Bind a Vertex Array Object. Any buffers after this will be bound to the VAO for later use.
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	createBuffers(data);

	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Vertex::pos)));
	glEnableVertexAttribArray(0);

	// Texture attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Vertex::uv)));
	glEnableVertexAttribArray(1);

	// Normal attribute
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Vertex::normal)));
	glEnableVertexAttribArray(2);

	// Unbind from the VAO
	glBindVertexArray(0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Mesh::createBuffers(const BufferData& data)
{
	vertexCount = static_cast<unsigned int>(data.vertices.size());

	// Create a Vertex Buffer Object
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	// Upload vertex data to OpenGL (bound to the VAO now)
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertexCount, data.vertices.data(), GL_STATIC_DRAW);
}

void Mesh::draw()
{
	// Count draw call
	DRAW_CALLS++;

	// Draw the mesh
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, vertexCount);
	glBindVertexArray(0);
}

Shader* Mesh::getShader() const
{
	return shader;
}

bool Mesh::setShader(Shader* shader)
{
	if (shader == nullptr)
	{
		std::cout << "Mesh Error: Invalid Shader selected!" << std::endl;
		return false;
	}

	this->shader = shader;
	return true;
}
