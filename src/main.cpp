#include "Window.h"
#include "Input.h"
#include "Shader.h"
#include "Mesh.h"
#include "IndexedMesh.h"
#include "Texture.h"
#include "FreeCamera.h"
#include "Time.h"
#include "Chunk.h"
#include "Tileset.h"
#include "Block.h"
#include "World.h"
#include "SlotMap.h"
#include "WorldGenerator.h"
#include "Biome.h"

#include <iostream>
#include <string>
#include <fstream> // for file reading
#include <vector>
#include <optional>
#include <ctime> // for srand() seed
#include <filesystem>
#include <memory>

#include <GLFW/glfw3.h>
#include <stb_image.h>
#include <stb_image_write.h>
#include <noise/noise.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <imgui.h>

// Read a file in the fastest way possible (using C++ streams), writing the
// file content into a string pointer and returning true/false depending on
// if the operation succeeded or failed.
std::optional<std::string> get_file_contents(const char *filename)
{
	std::ifstream in{ filename, std::ios::in | std::ios::binary };

	if (in)
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();

		return contents;
	}

	return std::nullopt; // Same as return {}
}

int errorCrash()
{
	std::cin.get();
	return -1;
}

void setWireframeMode(bool wireframeEnabled)
{
	if (wireframeEnabled)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

int main()
{
	// Random seed each time the program starts
	srand(static_cast<unsigned int>(time(0)));
	// srand(0);

	/// Create directory structure
	// if (!std::filesystem::is_directory("res"))
	// 	if (!std::filesystem::create_directory("res"))
	// 		std::cout << "Filesystem Error: Could not create res directory!" << std::endl;

	if(!std::filesystem::is_directory("res/mipmaps"))
		if (!std::filesystem::create_directory("res/mipmaps"))
			std::cout << "Filesystem Error: Could not create mipmaps directory inside res!" << std::endl;
	/// End directory structure

	// Flip any loaded textures due to OpenGL expecting the Y to be on the bottom.
	stbi_set_flip_vertically_on_load(true);
	stbi_flip_vertically_on_write(true);

	std::cout << "Creating OpenGL context..." << std::endl;

	Window window{ true, 100u};
	if (!window.open(1280, 720, "Voxel Game", 0))
		return errorCrash();

	int numAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &numAttributes);
	std::cout << "Maximum number of vertex attributes supported: " << numAttributes << std::endl;

	std::cout << "Loading textures..." << std::endl;

	// Load texture
	Tileset tileset{ "res/textures/image_tileset.png", 64 };
	if (!tileset.load(true, false, false))
		return errorCrash();

	// Fill the BLOCKS map with all blocks in the game
	glm::vec2 tilesetUVStep = tileset.getUVStep();
	Block::BLOCKS["Air"] = Block{ "Air", glm::vec2{ -1.0f, -1.0f } };
	Block::BLOCKS["Dirt"] = Block{ "Dirt", glm::vec2{ 0.0f, 0.0f } };
	Block::BLOCKS["Stone"] = Block{ "Stone", glm::vec2{ 2.0f, 1.0f } * tilesetUVStep };
	Block::BLOCKS["Sand"] = Block{ "Sand", glm::vec2{ 2.0f, 2.0f } * tilesetUVStep };
	Block::BLOCKS["Grass"] = Block{ "Grass", glm::vec2{ 0.0f, 2.0f } * tilesetUVStep };
	Block::BLOCKS["Water"] = Block{ "Water", glm::vec2{ 1.0f, 2.0f } * tilesetUVStep };
	Block::BLOCKS["Diamond Ore"] = Block{ "Diamond Ore", glm::vec2{ 1.0f, 1.0f } * tilesetUVStep };
	Block::BLOCKS["Emerald Ore"] = Block{ "Emerald Ore", glm::vec2{ 1.0f, 0.0f } * tilesetUVStep };
	Block::BLOCKS["Gold Ore"] = Block{ "Gold Ore", glm::vec2{ 2.0f, 0.0f } * tilesetUVStep };
	Block::BLOCKS["Iron Ore"] = Block{ "Iron Ore", glm::vec2{ 3.0f, 0.0f } * tilesetUVStep };

	std::cout << "Compiling shaders..." << std::endl;

	// Compile shader
	/*Shader basicShader{ &tileset, "res/shaders/basic.vs", "res/shaders/basic.fs" };
	if (!basicShader.compile())
		return errorCrash();
	basicShader.initialize();*/

	Shader lightShader{ &tileset, "res/shaders/light.vs", "res/shaders/light.fs" };
	if (!lightShader.compile())
		return errorCrash();
	lightShader.initialize();

	setWireframeMode(false);

	// Measures elapsed time since last tick (different to render time)
	DeltaTime tickTime;

	FreeCamera camera{ &window, &tickTime, 50.0f, 60.0f, 0.15f, 700.0f, glm::vec3{16 * 16, 85.0f, 16 * 16} };
	window.setCamera(&camera);
	glm::vec3 testPoint = camera.transform.position - camera.transform.forward() * 5.0f;

	std::cout << "Building chunks..." << std::endl;

	World world{ &lightShader, &tileset, &camera };
	world.preload(16, -4, 16, 32, 8, 32);

	std::cout << "Done loading!" << std::endl;
	window.reset();
	tickTime.reset();

	// Game loop
	while (window.isOpen())
	{
		// Do-while loop continues looping multiple times if the framerate is capped to a value
		// in order to continue updating window events and game code without drawing.
		do
		{
			// Calculate delta time
			tickTime.calc();

			// Calculate view & projection matrices, as well as movement & mouse look
			camera.update();

			//bool b = camera.frustumTestPoint(testPoint);
			//std::cout << b << std::endl;
			//float pointCameraZ = glm::dot(testPoint - camera.transform.position, glm::normalize(camera.transform.forward()));
			//std::cout << b << ": " << pointCameraZ << std::endl;

			// Input
			if (window.getInput().getKeyDown(GLFW_KEY_ESCAPE))
				window.close();

		} while (window.update(tickTime.getDelta()));

		/// DRAW
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Reset draw call counter every frame
		Mesh::DRAW_CALLS = 0;

		// Send camera position to chunk shader and draw all chunks
		//world.drawChunks();
		unsigned int chunksInFrustum = world.drawChunksInFrustum();

		// Draw UI
		window.startUIFrame();

		const ImVec4 textColor{ 0.7f, 0.8f, 1.0f, 1.0f };

		ImGui::SetNextWindowPos(ImVec2{ 20.0f, 20.0f });
		ImGui::Begin("", nullptr, ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_AlwaysAutoResize);

		auto camera_pos = camera.transform.position;
		auto weightsAtCamera = world.getGenerator().getWeights(static_cast<int>(std::roundf(camera_pos.x)), static_cast<int>(std::roundf(camera_pos.z)));
		auto& biomes = world.getGenerator().getBiomes();

		std::string framerateText = "FPS: " + std::to_string(window.getFramerate());
		std::string drawCallText = "Draw Calls: " + std::to_string(Mesh::DRAW_CALLS);
		std::string chunksInFrustumText = "Chunks in Frustum: " + std::to_string(chunksInFrustum);
		std::string biomeText = "Biome: " + biomes[weightsAtCamera.second]->getName();
		std::string weightsText;

		for (int i = 0; i < weightsAtCamera.first.size(); i++)
		{
			weightsText += biomes[i]->getName() + "W=" + std::to_string(weightsAtCamera.first[i]) + " ";
		}

		std::string cameraPos;
		cameraPos += "X:" + std::to_string(camera_pos.x) + " ";
		cameraPos += "Y:" + std::to_string(camera_pos.y) + " ";
		cameraPos += "Z:" + std::to_string(camera_pos.z);

		ImGui::TextColored(textColor, framerateText.c_str());
		ImGui::TextColored(textColor, drawCallText.c_str());
		ImGui::TextColored(textColor, chunksInFrustumText.c_str());
		ImGui::TextColored(textColor, biomeText.c_str());
		ImGui::TextColored(textColor, weightsText.c_str());
		ImGui::TextColored(textColor, cameraPos.c_str());

		ImGui::End();

		// Draw call to ImGui and swap buffers
		window.endUIFrame();
		window.swapBuffers();

		window.measureFramerate();
		/// END DRAW
	}

	return 0;
}
