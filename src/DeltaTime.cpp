#include "DeltaTime.h"

#include <GLFW/glfw3.h>

DeltaTime::DeltaTime(float timeScale)
{
	this->timeScale = timeScale;
	this->deltaTime = 0.0f;
}

DeltaTime::~DeltaTime()
{
}

void DeltaTime::reset()
{
	float time = static_cast<float>(glfwGetTime());
	deltaTime = 0.0f;
	lastTime = time;
}

void DeltaTime::calc()
{
	// Calculate delta time
	float time = static_cast<float>(glfwGetTime());
	deltaTime = (time - lastTime) * timeScale;
	lastTime = time;
}