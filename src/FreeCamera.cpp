#include "FreeCamera.h"

#include "Input.h"
#include "Transform.h"

#include <iostream>

FreeCamera::FreeCamera(Window* window, DeltaTime* time, float moveSpeed, float fov, float nearPlane, float farPlane, glm::vec3 position) 
	: Camera(window, fov, nearPlane, farPlane, position)
{
	this->time = time;
	this->moveSpeed = moveSpeed;

	// Default camera orientation
	yaw = glm::radians(215.0f);
	pitch = glm::radians(-35.0f);

	//transform.orientation = glm::quatLookAt(glm::vec3{ 1.0f, 0.0f, 1.0f }, glm::vec3{ 0.0f, 1.0f, 0.0f });

	//yaw = 0.0f;
	//pitch = 0.0f;
}

FreeCamera::~FreeCamera()
{
} 

void FreeCamera::update()
{
	Input& input = window->getInput();
	float deltaTime = time->getDelta();
	
	// Move twice as fast when holding down shift
	float speedScale = input.getKeyDown(GLFW_KEY_LEFT_SHIFT) ? 2.0f : 1.0f;

	glm::vec3 dir{ 0.0f, 0.0f, 0.0f };

	if (input.getKeyDown(GLFW_KEY_W))
		dir += transform.forward();
	if (input.getKeyDown(GLFW_KEY_S))
		dir += transform.forward() * -1.0f;
	if (input.getKeyDown(GLFW_KEY_A))
		dir += transform.right() * -1.0f;
	if (input.getKeyDown(GLFW_KEY_D))
		dir += transform.right();

	if (input.getKeyDown(GLFW_KEY_SPACE))
		dir += WORLD_UP;
	if (input.getKeyDown(GLFW_KEY_LEFT_CONTROL))
		dir += WORLD_DOWN;
	
	// Cannot normalize a 0-vector (or values close to 0)!
	if(dir.x != 0.0f || dir.y != 0.0f || dir.z != 0.0f)
		dir = glm::normalize(dir);
	
	//dir.y *= -1.0f;
	transform.position += dir * moveSpeed * speedScale * deltaTime;
	
	glm::vec2 mouseDelta = input.getMouseDelta() * -1.0f;
	pitch += mouseDelta.y;
	yaw += mouseDelta.x;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (pitch > glm::radians(89.0f))
		pitch = glm::radians(89.0f);
	if (pitch < glm::radians(-89.0f))
		pitch = glm::radians(-89.0f);

	// Don't let yaw get too high
	if (yaw > glm::radians(360.0f))
		yaw -= glm::radians(360.0f);
	if (yaw < glm::radians(-360.0f))
		yaw += glm::radians(360.0f);

	// Convert pitch & yaw to quaternion
	transform.setEulerAngles(glm::vec3{ glm::degrees(pitch), glm::degrees(yaw), glm::degrees(0.0f) });
	//orientation = glm::yawPitchRoll(yaw, pitch, 0.0f);

	//std::cout << "Pitch: " << glm::degrees(pitch) << " | Yaw: " << glm::degrees(yaw) << std::endl;

	// Call base for calculating view & projecton matrices
	Camera::update();
}
