#include "Chunk.h"

#include <glm/vec2.hpp>
#include <iostream>
#include <optional>
#include <utility> // std::pair

#include "World.h"
#include "WorldGenerator.h"
#include "Biome.h"

Chunk::Chunk()
	: mesh(nullptr), world(nullptr), loaded(false)
{
}

Chunk::Chunk(const ChunkKey& key, World* world)
	: key(key), mesh(world->chunkShader), world(world), loaded(false),
	// Assign keys of neighbour chunks (they are not guaranteed to exist)
	frontChunk(key + ChunkKey(0, 0, 1)), backChunk(key + ChunkKey(0, 0, -1)),
	leftChunk(key + ChunkKey(-1, 0, 0)), rightChunk(key + ChunkKey(1, 0, 0)),
	upChunk(key + ChunkKey(0, 1, 0)), downChunk(key + ChunkKey(0, -1, 0))
{
}

Chunk::~Chunk()
{
	delete[] blocks;
}

std::vector<glm::vec3> Chunk::getCorners() const
{
	std::vector<glm::vec3> corners {};

	// Front bottom left (origin)
	glm::vec3 fBotL{ mesh.transform.position.x /*- BLOCK_HALF*/, mesh.transform.position.y /*- BLOCK_HALF*/, mesh.transform.position.z /*+ BLOCK_HALF*/ };


	// Front bottom right
	glm::vec3 fBotR = fBotL + glm::vec3{ CHUNK_SIZE, 0.0f, 0.0f };

	// Front top left
	glm::vec3 fTopL = fBotL + glm::vec3{ 0.0f, CHUNK_SIZE, 0.0f };

	// Front top right
	glm::vec3 fTopR = fTopL + glm::vec3{ CHUNK_SIZE, 0.0f, 0.0f };

	// Back corners
	glm::vec3 bBotL = fBotL + glm::vec3{ 0.0f, 0.0f, -CHUNK_SIZE };
	glm::vec3 bBotR = fBotR + glm::vec3{ 0.0f, 0.0f, -CHUNK_SIZE };
	glm::vec3 bTopL = fTopL + glm::vec3{ 0.0f, 0.0f, -CHUNK_SIZE };
	glm::vec3 bTopR = fTopR + glm::vec3{ 0.0f, 0.0f, -CHUNK_SIZE };

	// Add corners and return them
	corners.emplace_back(fBotL);
	corners.emplace_back(fBotR);
	corners.emplace_back(fTopL);
	corners.emplace_back(fTopR);

	corners.emplace_back(bBotL);
	corners.emplace_back(bBotR);
	corners.emplace_back(bTopL);
	corners.emplace_back(bTopR);

	return corners;
}

void Chunk::load()
{
	blocks = new Block[CHUNK_ONE * CHUNK_ONE * CHUNK_ONE];

	for (int x = 0; x < CHUNK_ONE; x++)
	{
		int blockWorldPosX = key.x * CHUNK_ONE + x;

		for (int z = 0; z < CHUNK_ONE; z++)
		{
			int blockWorldPosZ = key.z * CHUNK_ONE + z;

			// Block height (2D) and overworld biome type
			auto height2D = world->generator.getBlockHeight(blockWorldPosX, blockWorldPosZ);
			//std::cout << height2D.first << std::endl;

			for (int y = 0; y < CHUNK_ONE; y++)
			{
				int blockWorldPosY = key.y * CHUNK_ONE + y;

				// Solid
				if (blockWorldPosY <= height2D.first)
					blocks[index(x, y, z)] = Block::BLOCKS[world->generator.getBiomes()[height2D.second]->getBlock()];
				// Air
				else
					blocks[index(x, y, z)] = Block::BLOCKS["Air"];
			}
		}
	}

	loaded = true;
}

void Chunk::build()
{
	// Neighbour chunks
	auto frontNeighbour = world->chunks.get(frontChunk);
	auto backNeighbour = world->chunks.get(backChunk);
	auto leftNeighbour = world->chunks.get(leftChunk);
	auto rightNeighbour = world->chunks.get(rightChunk);
	auto upNeighbour = world->chunks.get(upChunk);
	auto downNeighbour = world->chunks.get(downChunk);

	// Whether or not the neighbour chunks are loaded
	bool frontNeighbourValid = frontNeighbour != nullptr && *frontNeighbour;
	bool backNeighbourValid = backNeighbour != nullptr && *backNeighbour;
	bool leftNeighbourValid = leftNeighbour != nullptr && *leftNeighbour;
	bool rightNeighbourValid = rightNeighbour != nullptr && *rightNeighbour;
	bool upNeighbourValid = upNeighbour != nullptr && *upNeighbour;
	bool downNeighbourValid = downNeighbour != nullptr && *downNeighbour;

	for (int x = 0; x < CHUNK_ONE; x++)
	{
		for (int y = 0; y < CHUNK_ONE; y++)
		{
			for (int z = 0; z < CHUNK_ONE; z++)
			{
				Block& currentBlock = blocks[index(x, y, z)];

				if (currentBlock.name == "Air")
					continue;

				// Neighbour blocks of current chunk. Nullptr if at edge of chunk, i.e. does not exist
				Block* frontBlock = nullptr;
				Block* backBlock = nullptr;
				Block* leftBlock = nullptr;
				Block* rightBlock = nullptr;
				Block* upBlock = nullptr;
				Block* downBlock = nullptr;

				/// If neighbour blocks are not out of bounds, fetch them from the current chunk.
				/// Else, they are out of bounds and must be fetched from neighbour chunks.
				/// If they are nullptr, it means the block has not been loaded yet (outside currently loaded world)
				// TODO: Implement neighbour chunk checks here

				// Front neighbour block
				if (z + 1 < CHUNK_ONE)
					frontBlock = &blocks[index(x, y, z + 1)];
				else if (frontNeighbourValid)
					frontBlock = &frontNeighbour->value().getBlock(x, y, 0);

				// Back neighbour block
				if (z > 0)
					backBlock = &blocks[index(x, y, z - 1)];
				else if (backNeighbourValid)
					backBlock = &backNeighbour->value().getBlock(x, y, CHUNK_ONE - 1);

				// Right neighbour block
				if (x + 1 < CHUNK_ONE)
					rightBlock = &blocks[index(x + 1, y, z)];
				else if (rightNeighbourValid)
					rightBlock = &rightNeighbour->value().getBlock(0, y, z);

				// Left neighbour block
				if (x > 0)
					leftBlock = &blocks[index(x - 1, y, z)];
				else if (leftNeighbourValid)
					leftBlock = &leftNeighbour->value().getBlock(CHUNK_ONE - 1, y, z);

				// Up neighbour block
				if (y + 1 < CHUNK_ONE)
					upBlock = &blocks[index(x, y + 1, z)];
				else if (upNeighbourValid)
					upBlock = &upNeighbour->value().getBlock(x, 0, z);

				// Down neighbour block
				if (y > 0)
					downBlock = &blocks[index(x, y - 1, z)];
				else if (downNeighbourValid)
					downBlock = &downNeighbour->value().getBlock(x, CHUNK_ONE - 1, z);

				/// If a neighbour is nullptr or air, add a face to that side:

				if (frontBlock == nullptr || frontBlock->name == "Air")
					FaceFront(x, y, z, currentBlock.uv);
				if (backBlock == nullptr || backBlock->name == "Air")
					FaceBack(x, y, z, currentBlock.uv);
				if (rightBlock == nullptr || rightBlock->name == "Air")
					FaceRight(x, y, z, currentBlock.uv);
				if (leftBlock == nullptr || leftBlock->name == "Air")
					FaceLeft(x, y, z, currentBlock.uv);
				if (upBlock == nullptr || upBlock->name == "Air")
					FaceUp(x, y, z, currentBlock.uv);
				if (downBlock == nullptr || downBlock->name == "Air")
					FaceDown(x, y, z, currentBlock.uv);
			}
		}
	}

	// Build this chunk's mesh
	mesh.build(bufferData);
	bufferData.clear();
}

void Chunk::rebuildNeighbours()
{
	// Neighbour chunks
	auto frontNeighbour = world->chunks.get(frontChunk);
	auto backNeighbour = world->chunks.get(backChunk);
	auto leftNeighbour = world->chunks.get(leftChunk);
	auto rightNeighbour = world->chunks.get(rightChunk);
	auto upNeighbour = world->chunks.get(upChunk);
	auto downNeighbour = world->chunks.get(downChunk);

	// Check if neighbour chunks are valid and if they have loaded data, then rebuild them
	if (frontNeighbour != nullptr && *frontNeighbour && frontNeighbour->value().loaded)
		frontNeighbour->value().build();
	if (backNeighbour != nullptr && *backNeighbour && backNeighbour->value().loaded)
		backNeighbour->value().build();
	if (leftNeighbour != nullptr && *leftNeighbour && leftNeighbour->value().loaded)
		leftNeighbour->value().build();
	if (rightNeighbour != nullptr && *rightNeighbour && rightNeighbour->value().loaded)
		rightNeighbour->value().build();
	if (upNeighbour != nullptr && *upNeighbour && upNeighbour->value().loaded)
		upNeighbour->value().build();
	if (downNeighbour != nullptr && *downNeighbour && downNeighbour->value().loaded)
		downNeighbour->value().build();
}

void Chunk::FaceFront(int ix, int iy, int iz, glm::vec2 uv)
{
	float x = static_cast<float>(ix);
	float y = static_cast<float>(iy);
	float z = static_cast<float>(iz);

	// The uv parameter supplied is the bottom-left uv coords of a tile,
	// thus it must be adjusted for the other 3 corners using the tileset's uv step.
	glm::vec2 uvStep = world->chunkTileset->getUVStep();

	// Half pixel correction is applied, so the UVs point to the center of a pixel, instead of the edge,
	// which would lead to pixel bleeding due to float rounding-errors.

	Vertex topLeft{ glm::vec3{x - BLOCK_HALF, y + BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{0.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTL(), glm::vec3{ 0.0f, 0.0f, 1.0f } };

	Vertex topRight{ glm::vec3{x + BLOCK_HALF, y + BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTR(), glm::vec3{ 0.0f, 0.0f, 1.0f } };

	Vertex bottomLeft{ glm::vec3{x - BLOCK_HALF, y - BLOCK_HALF, z + BLOCK_HALF},
					uv /*+ glm::vec2{0.0f, 0.0f} * uvStep*/ + world->chunkTileset->getHalfPixelBL(), glm::vec3{ 0.0f, 0.0f, 1.0f } };

	Vertex bottomRight{ glm::vec3{x + BLOCK_HALF, y - BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 0.0f} * uvStep + world->chunkTileset->getHalfPixelBR(), glm::vec3{ 0.0f, 0.0f, 1.0f } };

	// Form a quad/face with two triangles (note the counter-clockwise order)
	Vertex vertices[6] = {
		// Top triangle
		topRight, topLeft, bottomLeft,
		// Bottom triangle
		bottomLeft, bottomRight, topRight
	};

	// Copy vertices to the buffer data object for further use
	bufferData.vertices.insert(bufferData.vertices.end(), std::begin(vertices), std::end(vertices));
}

void Chunk::FaceBack(int ix, int iy, int iz, glm::vec2 uv)
{
	float x = static_cast<float>(ix);
	float y = static_cast<float>(iy);
	float z = static_cast<float>(iz);

	// The uv parameter supplied is the bottom-left uv coords of a tile,
	// thus it must be adjusted for the other 3 corners using the tileset's uv step.
	glm::vec2 uvStep = world->chunkTileset->getUVStep();

	// Half pixel correction is applied, so the UVs point to the center of a pixel, instead of the edge,
	// which would lead to pixel bleeding due to float rounding-errors.

	Vertex topLeft{ glm::vec3{x - BLOCK_HALF, y + BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{0.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTL(), glm::vec3{ 0.0f, 0.0f, -1.0f } };

	Vertex topRight{ glm::vec3{x + BLOCK_HALF, y + BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{1.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTR(), glm::vec3{ 0.0f, 0.0f, -1.0f } };

	Vertex bottomLeft{ glm::vec3{x - BLOCK_HALF, y - BLOCK_HALF, z - BLOCK_HALF},
					uv /*+ glm::vec2{0.0f, 0.0f} * uvStep*/ + world->chunkTileset->getHalfPixelBL(), glm::vec3{ 0.0f, 0.0f, -1.0f } };

	Vertex bottomRight{ glm::vec3{x + BLOCK_HALF, y - BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{1.0f, 0.0f} * uvStep + world->chunkTileset->getHalfPixelBR(), glm::vec3{ 0.0f, 0.0f, -1.0f } };

	// Form a quad/face with two triangles (note the counter-clockwise order)
	Vertex vertices[6] = {
		// Top triangle
		bottomLeft, topLeft, topRight,//topRight, topLeft, bottomLeft,
		// Bottom triangle
		topRight, bottomRight, bottomLeft//bottomLeft, bottomRight, topRight
	};

	// Copy vertices to the buffer data object for further use
	bufferData.vertices.insert(bufferData.vertices.end(), std::begin(vertices), std::end(vertices));
}

void Chunk::FaceLeft(int ix, int iy, int iz, glm::vec2 uv)
{
	float x = static_cast<float>(ix);
	float y = static_cast<float>(iy);
	float z = static_cast<float>(iz);

	// The uv parameter supplied is the bottom-left uv coords of a tile,
	// thus it must be adjusted for the other 3 corners using the tileset's uv step.
	glm::vec2 uvStep = world->chunkTileset->getUVStep();

	// Half pixel correction is applied, so the UVs point to the center of a pixel, instead of the edge,
	// which would lead to pixel bleeding due to float rounding-errors.

	Vertex topLeft{ glm::vec3{x - BLOCK_HALF, y + BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{0.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTL(), glm::vec3{ -1.0f, 0.0f, 0.0f } };

	Vertex topRight{ glm::vec3{x - BLOCK_HALF, y + BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTR(), glm::vec3{ -1.0f, 0.0f, 0.0f } };

	Vertex bottomLeft{ glm::vec3{x - BLOCK_HALF, y - BLOCK_HALF, z - BLOCK_HALF},
					uv /*+ glm::vec2{0.0f, 0.0f} * uvStep*/ + world->chunkTileset->getHalfPixelBL(), glm::vec3{ -1.0f, 0.0f, 0.0f } };

	Vertex bottomRight{ glm::vec3{x - BLOCK_HALF, y - BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 0.0f} * uvStep + world->chunkTileset->getHalfPixelBR(), glm::vec3{ -1.0f, 0.0f, 0.0f } };

	// Form a quad/face with two triangles (note the counter-clockwise order)
	Vertex vertices[6] = {
		// Top triangle
		topRight, topLeft, bottomLeft,
		// Bottom triangle
		bottomLeft, bottomRight, topRight
	};

	// Copy vertices to the buffer data object for further use
	bufferData.vertices.insert(bufferData.vertices.end(), std::begin(vertices), std::end(vertices));
}

void Chunk::FaceRight(int ix, int iy, int iz, glm::vec2 uv)
{
	float x = static_cast<float>(ix);
	float y = static_cast<float>(iy);
	float z = static_cast<float>(iz);

	// The uv parameter supplied is the bottom-left uv coords of a tile,
	// thus it must be adjusted for the other 3 corners using the tileset's uv step.
	glm::vec2 uvStep = world->chunkTileset->getUVStep();

	// Half pixel correction is applied, so the UVs point to the center of a pixel, instead of the edge,
	// which would lead to pixel bleeding due to float rounding-errors.

	Vertex topLeft{ glm::vec3{x + BLOCK_HALF, y + BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{0.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTL(), glm::vec3{ 1.0f, 0.0f, 0.0f } };

	Vertex topRight{ glm::vec3{x + BLOCK_HALF, y + BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTR(), glm::vec3{ 1.0f, 0.0f, 0.0f } };

	Vertex bottomLeft{ glm::vec3{x + BLOCK_HALF, y - BLOCK_HALF, z - BLOCK_HALF},
					uv /*+ glm::vec2{0.0f, 0.0f} * uvStep*/ + world->chunkTileset->getHalfPixelBL(), glm::vec3{ 1.0f, 0.0f, 0.0f } };

	Vertex bottomRight{ glm::vec3{x + BLOCK_HALF, y - BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 0.0f} * uvStep + world->chunkTileset->getHalfPixelBR(), glm::vec3{ 1.0f, 0.0f, 0.0f } };

	// Form a quad/face with two triangles (note the counter-clockwise order)
	Vertex vertices[6] = {
		// Top triangle
		bottomLeft, topLeft, topRight,//topRight, topLeft, bottomLeft,
		// Bottom triangle
		topRight, bottomRight, bottomLeft//bottomLeft, bottomRight, topRight
	};

	// Copy vertices to the buffer data object for further use
	bufferData.vertices.insert(bufferData.vertices.end(), std::begin(vertices), std::end(vertices));
}

void Chunk::FaceUp(int ix, int iy, int iz, glm::vec2 uv)
{
	float x = static_cast<float>(ix);
	float y = static_cast<float>(iy);
	float z = static_cast<float>(iz);

	// The uv parameter supplied is the bottom-left uv coords of a tile,
	// thus it must be adjusted for the other 3 corners using the tileset's uv step.
	glm::vec2 uvStep = world->chunkTileset->getUVStep();

	// Half pixel correction is applied, so the UVs point to the center of a pixel, instead of the edge,
	// which would lead to pixel bleeding due to float rounding-errors.

	Vertex topLeft{ glm::vec3{x - BLOCK_HALF, y + BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{0.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTL(), glm::vec3{ 0.0f, 1.0f, 0.0f } };

	Vertex topRight{ glm::vec3{x + BLOCK_HALF, y + BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{1.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTR(), glm::vec3{ 0.0f, 1.0f, 0.0f } };

	Vertex bottomLeft{ glm::vec3{x - BLOCK_HALF, y + BLOCK_HALF, z + BLOCK_HALF},
					uv /*+ glm::vec2{0.0f, 0.0f} * uvStep*/ + world->chunkTileset->getHalfPixelBL(), glm::vec3{ 0.0f, 1.0f, 0.0f } };

	Vertex bottomRight{ glm::vec3{x + BLOCK_HALF, y + BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 0.0f} * uvStep + world->chunkTileset->getHalfPixelBR(), glm::vec3{ 0.0f, 1.0f, 0.0f } };

	// Form a quad/face with two triangles (note the counter-clockwise order)
	Vertex vertices[6] = {
		// Top triangle
		topRight, topLeft, bottomLeft,
		// Bottom triangle
		bottomLeft, bottomRight, topRight
	};

	// Copy vertices to the buffer data object for further use
	bufferData.vertices.insert(bufferData.vertices.end(), std::begin(vertices), std::end(vertices));
}

void Chunk::FaceDown(int ix, int iy, int iz, glm::vec2 uv)
{
	float x = static_cast<float>(ix);
	float y = static_cast<float>(iy);
	float z = static_cast<float>(iz);

	// The uv parameter supplied is the bottom-left uv coords of a tile,
	// thus it must be adjusted for the other 3 corners using the tileset's uv step.
	glm::vec2 uvStep = world->chunkTileset->getUVStep();

	// Half pixel correction is applied, so the UVs point to the center of a pixel, instead of the edge,
	// which would lead to pixel bleeding due to float rounding-errors.

	Vertex topLeft{ glm::vec3{x - BLOCK_HALF, y - BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{0.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTL(), glm::vec3{ 0.0f, -1.0f, 0.0f } };

	Vertex topRight{ glm::vec3{x + BLOCK_HALF, y - BLOCK_HALF, z - BLOCK_HALF},
					uv + glm::vec2{1.0f, 1.0f} * uvStep + world->chunkTileset->getHalfPixelTR(), glm::vec3{ 0.0f, -1.0f, 0.0f } };

	Vertex bottomLeft{ glm::vec3{x - BLOCK_HALF, y - BLOCK_HALF, z + BLOCK_HALF},
					uv /*+ glm::vec2{0.0f, 0.0f} * uvStep*/ + world->chunkTileset->getHalfPixelBL(), glm::vec3{ 0.0f, -1.0f, 0.0f } };

	Vertex bottomRight{ glm::vec3{x + BLOCK_HALF, y - BLOCK_HALF, z + BLOCK_HALF},
					uv + glm::vec2{1.0f, 0.0f} * uvStep + world->chunkTileset->getHalfPixelBR(), glm::vec3{ 0.0f, -1.0f, 0.0f } };

	// Form a quad/face with two triangles (note the counter-clockwise order)
	Vertex vertices[6] = {
		// Top triangle
		bottomLeft, topLeft, topRight,//topRight, topLeft, bottomLeft,
		// Bottom triangle
		topRight, bottomRight, bottomLeft//bottomLeft, bottomRight, topRight
	};

	// Copy vertices to the buffer data object for further use
	bufferData.vertices.insert(bufferData.vertices.end(), std::begin(vertices), std::end(vertices));
}

void Chunk::print()
{
	for (int x = 0; x < CHUNK_ONE; x++)
	{
		for (int y = 0; y < CHUNK_ONE; y++)
		{
			for (int z = 0; z < CHUNK_ONE; z++)
			{
				//std::cout << blocks[x][y][z].name << std::endl;
				std::cout << blocks[index(x, y, z)].name << std::endl;
			}
		}
	}
}
