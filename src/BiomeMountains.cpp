#include "BiomeMountains.h"

gen::biome::Mountains::Mountains(Seed* seed)
	: Biome(seed, "Mountains", "Grass",
		Ecocline{ 0.0, 0.0, 1.0 })
{
	perlinMap.SetSeed(seed->getNext());
	perlinMap.SetOctaveCount(4);
	perlinMap.SetFrequency(0.05);
	perlinMap.SetPersistence(0.45);

	clampMod.SetSourceModule(0, perlinMap);
	clampMod.SetBounds(0.0, 1.0);
}

gen::biome::Mountains::~Mountains()
{
}

double gen::biome::Mountains::getValue(const NoiseCoords& coords)
{
	return clampMod.GetValue(coords.x, coords.y, coords.z);
}