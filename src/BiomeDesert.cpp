#include "BiomeDesert.h"

gen::biome::Desert::Desert(Seed* seed)
	: Biome(seed, "Desert", "Sand",
		Ecocline{ 1.0, -1.0, 0.0 })
{
	perlinMap.SetSeed(seed->getNext());
	perlinMap.SetOctaveCount(4);
	perlinMap.SetFrequency(0.15);
	perlinMap.SetPersistence(0.45);

	clampMod.SetSourceModule(0, perlinMap);
	clampMod.SetBounds(-0.7, -0.3);
}

gen::biome::Desert::~Desert()
{
}

double gen::biome::Desert::getValue(const NoiseCoords& coords)
{
	return clampMod.GetValue(coords.x, coords.y, coords.z);
}