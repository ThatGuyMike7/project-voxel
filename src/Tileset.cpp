#include "Tileset.h"

#include "Mipmaps.h"

#include <iostream> // For debug
#include <glad/glad.h>

#include <filesystem>
#include <stb_image.h>

Tileset::Tileset(const std::string& texturePath, int resolution) : Texture(texturePath)
{
	this->resolution = resolution;
}

Tileset::~Tileset()
{
}

void Tileset::configure(BYTE* data, GLint wrapMode, GLint minFilter, GLint maxFilter)
{
	Texture::configure(data, wrapMode, minFilter, maxFilter);

	unsigned int mipmapCount = 6;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipmapCount);

	// If mipmap files do not exist on disk...
	if (!std::filesystem::exists("res/mipmaps/atlas_mipmap_1.png") ||
		!std::filesystem::exists("res/mipmaps/atlas_mipmap_2.png") || 
		!std::filesystem::exists("res/mipmaps/atlas_mipmap_3.png") || 
		!std::filesystem::exists("res/mipmaps/atlas_mipmap_4.png") || 
		!std::filesystem::exists("res/mipmaps/atlas_mipmap_5.png") || 
		!std::filesystem::exists("res/mipmaps/atlas_mipmap_6.png"))
	{
		std::cout << "Generating mipmaps..." << std::endl;

		// Generate mipmaps of the tileset and pass them to OpenGL
		generateMipmaps(data, mipmapCount);
	}
	// If mipmap files are on disk...
	else
	{
		std::cout << "Loading mipmaps..." << std::endl;

		// Load them from file
		for (unsigned int i = 1; i <= mipmapCount; i++)
		{
			std::string mipmapPath = "res/mipmaps/atlas_mipmap_" + std::to_string(i) + ".png";

			int numChannels;
			int mipmapWidth, mipmapHeight;
			BYTE* mipmap = stbi_load(mipmapPath.c_str(), &mipmapWidth, &mipmapHeight, &numChannels, STBI_rgb_alpha);

			// Upload mipmap to OpenGL, specifying the correct mipmap level (here: `i`)
			glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, mipmapWidth, mipmapHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmap);
		}
	}
}

void Tileset::generateMipmaps(BYTE* data, unsigned int count)
{
	BYTE* curData = data;
	int curWidth = width;
	int curHeight = height;
	int curTileRes = resolution;

	std::vector<BYTE*> toDelete;

	for (unsigned int i = 1; i <= count; i++)
	{
		BYTE* mipmap = mipmap_generateAtlas(curData, 4u, curWidth, curHeight, curTileRes, "atlas_mipmap_" + std::to_string(i));
		
		curWidth /= 2;
		curHeight /= 2;
		curTileRes /= 2;

		// Upload mipmap to OpenGL, specifying the correct mipmap level (here: `i`)
		glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, curWidth, curHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mipmap);

		// Delete the mipmap data later when it is no longer needed
		toDelete.emplace_back(mipmap);

		// Generate the next mipmap from the current mipmap
		curData = mipmap;

		// Cannot generate lower resolution mipmap than 1 pixel per tile
		if (curTileRes == 1 && i < count)
		{
			std::cout << "Tileset Warning: Cannot generate lower resolution mipmap than 1 pixel per tile!" << std::endl;
			break;
		}
	}

	// Delete all mipmaps from memory, they are now on the GPU
	for (unsigned int i = 0; i < toDelete.size(); i++)
	{
		BYTE*& d = toDelete.at(i);
		delete[] d;
		d = nullptr;
	}
}

void Tileset::postLoad()
{
	float resolutionF = static_cast<float>(this->resolution);

	// Calculate the correct UV step of this tileset
	uvStep = glm::vec2{ 
		resolutionF / static_cast<float>(width),
		resolutionF / static_cast<float>(height)
	};

	glm::vec2 halfPixel = glm::vec2{
		0.5f / static_cast<float>(width),
		0.5f / static_cast<float>(height)
	};

	halfPixelTL = glm::vec2{ halfPixel.x, -halfPixel.y };
	halfPixelTR = glm::vec2{ -halfPixel.x, -halfPixel.y };
	halfPixelBL = glm::vec2{ halfPixel.x, halfPixel.y };
	halfPixelBR = glm::vec2{ -halfPixel.x, halfPixel.y };
}