#include "Biome.h"
#include "Chunk.h" // for `CHUNK_ONE`

gen::biome::Biome::Biome(Seed* seed, const std::string& name, const std::string& blockName, const Ecocline& ecocline)
	: seed(seed), name(name), blockName(blockName), ecocline(ecocline)
{
}

gen::biome::Biome::~Biome()
{

}

// Returns noise coordinates from a 3D block position
gen::NoiseCoords gen::NoiseCoords::fromBlock(int blockX, int blockY, int blockZ)
{
	NoiseCoords coords = NoiseCoords::fromBlock(blockX, blockZ);

	double dY = static_cast<double>(blockY);
	double noiseY = dY / CHUNK_ONE;

	coords.y = noiseY;
	return coords;
}

// Returns noise coordinates from a 2D block position
gen::NoiseCoords gen::NoiseCoords::fromBlock(int blockX, int blockZ)
{
	double dX = static_cast<double>(blockX);
	double dZ = static_cast<double>(blockZ);

	double noiseX = dX / CHUNK_ONE;
	double noiseZ = dZ / CHUNK_ONE;

	return NoiseCoords{ noiseX, 0.0, noiseZ };
}
